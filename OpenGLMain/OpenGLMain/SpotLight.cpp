#include "SpotLight.h"

#include "Material.h"
#include "GameObject.h"
#include "Transform.h"


namespace engine
{
  DECLARE_COMPONENT(SpotLight)

  SpotLight::SpotLight()
  {
  }

  SpotLight::~SpotLight()
  {
  }

  void SpotLight::BeforRender(const RenderingContext & context)
  {
    Material::SpotLightInfo s;
    auto w_pos = GetGameObject()->FindComponent<Transform>()->GetPosition();
    s.position[0] = w_pos.x;
    s.position[1] = w_pos.y;
    s.position[2] = w_pos.z;
    glm::vec4 l_forward(0.0f, 0.0f, -1.0f, 0.0f);
    glm::vec4 w_forward = GetGameObject()->FindComponent<Transform>()->BuildL2WMatrix() * l_forward;
    s.direction[0] = w_forward.x;
    s.direction[1] = w_forward.y;
    s.direction[2] = w_forward.z;
    s.color[0] = color.x;
    s.color[1] = color.y;
    s.color[2] = color.z;
    s.color[3] = color.w;
    s.cutOff = cutOff;
    s.outerCutOff = outerCutOff;
    s.constant = constant;
    s.linear = linear;
    s.quadratic = quadratic;
    Material::PutSpotLightInfo(s);
  }
}