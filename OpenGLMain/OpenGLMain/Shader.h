#pragma once

#include <memory>

#include "ShaderData.h"


namespace engine
{
	class Shader
	{
  public:
    // cull
    bool cullFace;
    int cullFaceMode;
    // z
    bool zWrite;
    int zFunc;
    // blend
    bool blend;
    int rgbSFactor;
    int rgbDFactor;
    int rgbBlendQuation;
    int aSFactor;
    int aDFactor;
    int aBlendQuation;
    // stencil
    bool stencil;
    int stencilFunc;
    int stencilRef;
    int stencilMask;
    int stencilSFailOp;
    int stencilDFailOp;
    int stencilPassOp;

		Shader(std::shared_ptr<ShaderData> _vertShaderData, std::shared_ptr<ShaderData> _geomShaderData, std::shared_ptr<ShaderData> _fragShaderData);
    ~Shader();

    GLuint GetShaderProgram() const { return shaderProgram; }

    const std::string & GetRenderPassName() const { return renderPassName; }
    void SetRenderPassName(const std::string & _renderPassName) { renderPassName = _renderPassName; }

    int GetOrder() const { return order; }
    void SetOrder(int _order) { order = _order; }

	private:
    std::weak_ptr<ShaderData> vertShaderData;
    std::weak_ptr<ShaderData> geomShaderData;
    std::weak_ptr<ShaderData> fragShaderData;
    GLuint shaderProgram;
    std::string renderPassName;
    int order;

    GLuint CreateShader(GLenum shaderType, std::shared_ptr<ShaderData> shaderData);
	};
}

