#pragma once

#include "MeshData.h"


namespace engine
{
  class CubeMeshData : MeshData
  {
  public:
    CubeMeshData(float scale = 1.0f);
    ~CubeMeshData() override;
  };
}

