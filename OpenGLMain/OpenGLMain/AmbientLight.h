#pragma once

#include "BaseLight.h"

namespace engine
{
  class AmbientLight : public BaseLight
  {
    DEFINE_COMPONENT(AmbientLight, BaseLight)

  public:
    AmbientLight();
    ~AmbientLight() override;

    void BeforRender(const RenderingContext & context) override;
  };
}

