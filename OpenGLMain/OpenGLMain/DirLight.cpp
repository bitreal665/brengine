#include "DirLight.h"

#include "Material.h"
#include "GameObject.h"
#include "Transform.h"


namespace engine
{
  DECLARE_COMPONENT(DirLight)

  DirLight::DirLight()
  {
  }

  DirLight::~DirLight()
  {
  }

  void DirLight::BeforRender(const RenderingContext & context)
  {
    Material::DirLightInfo s;
    glm::vec4 l_forward(0.0f, 0.0f, -1.0f, 0.0f);
    glm::vec4 w_forward = GetGameObject()->FindComponent<Transform>()->BuildL2WMatrix() * l_forward;
    s.direction[0] = w_forward.x;
    s.direction[1] = w_forward.y;
    s.direction[2] = w_forward.z;
    s.color[0] = color.x;
    s.color[1] = color.y;
    s.color[2] = color.z;
    s.color[3] = color.w;
    Material::PutDirLightInfo(s);
  }
}