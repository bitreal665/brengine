#pragma once

#include <memory>
#include <string>

#include <assimp/scene.h>

#include "MeshData.h"

namespace engine
{
  class AssimpMeshData : MeshData
  {
  public:
    static void ParseMeshes(const std::string & fileName, std::vector<std::shared_ptr<MeshData>> & result);

    AssimpMeshData(const aiMesh * mesh);
    ~AssimpMeshData() override;

  private:
    static void ProcessNode(const aiScene * scene, const aiNode * node, std::vector<std::shared_ptr<MeshData>> & result);
  };
}
