#pragma once

#include <memory>
#include <vector>
#include <unordered_map>

#include <glm.hpp>

#include "Shader.h"
#include "AbstractTexture.h"
#include "RenderingContext.h"


namespace engine
{
	class Material
	{
  public:

    struct TextureSettings
    {
      std::shared_ptr<AbstractTexture> texture;
      std::string uniformName;

      TextureSettings::TextureSettings(const std::string & _uniformName, std::shared_ptr<AbstractTexture> _texture)
      {
        texture = _texture;
        uniformName = _uniformName;
      }
    };
    
    // === ubo section ===

    struct MiscInfo
    {
      float time;

      float _space[3];
    };

    struct MatrixInfo
    {
      glm::mat4x4 projection;
      glm::mat4x4 view;
      float viewPos [3 + 1];
    };

    struct AmbientLightInfo
    {
      float color[4];
    };

    struct DirLightInfo
    {
      float direction[3 + 1];
      float color[4];
    };

    struct PointLightInfo
    {
      float position[3 + 1];
      float color[4];
      float constant;
      float linear;
      float quadratic;
      float _space[1];
    };

    struct SpotLightInfo
    {
      float position[3 + 1];
      float direction[3 + 1];
      float color[4];
      float cutOff;
      float outerCutOff;
      float constant;
      float linear;
      float quadratic;
      float _space[3];
    };

    static GLuint UBO;

    static void InitUniformBuffer();
    static void DeinitUniformBuffer();

    static void PutMiscInfo(const MiscInfo & s);
    static void PutMatrixInfo(const MatrixInfo & s);
    static void PutAmbientLightInfo(const AmbientLightInfo & s);
    static void PutDirLightInfo(const DirLightInfo & s);
    static void PutPointLightInfo(const PointLightInfo & s);
    static void PutSpotLightInfo(const SpotLightInfo & s);

    // ===

		Material();
		~Material();

    void UpdateUVec4(const std::string & uName, const glm::vec4 & v) { uVec4[uName] = v; }
    void UpdateUTex(const std::string & uName, std::shared_ptr<AbstractTexture> v) { uTex[uName] = v; }
    
    void AddShader(std::shared_ptr<Shader> shader);

    bool WillRender(const RenderingContext & context) const;
    int GetOrder(const RenderingContext & context) const;

		void Render(RenderingContext & context) const;

	private:
    std::vector<std::shared_ptr<Shader>> shaders;

    std::unordered_map<std::string, std::shared_ptr<AbstractTexture>> uTex;
    std::unordered_map<std::string, glm::vec4> uVec4;

    void UpdateUniform(std::shared_ptr<Shader> shader, const std::string & uName, const glm::vec4 & v) const;
    void UpdateUniform(std::shared_ptr<Shader> shader, const std::string & uName, const glm::mat3x3 & v) const;
    void UpdateUniform(std::shared_ptr<Shader> shader, const std::string & uName, const glm::mat4x4 & v) const;
	};
}
