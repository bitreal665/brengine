#include "ShaderData.h"

#include <fstream>
#include <iostream>
#include <sstream>


namespace engine
{
  ShaderData::ShaderData(const std::string & _fileName) :
    fileName(_fileName)
  {
    std::string shaderCode;
    std::ifstream shaderFile;
    // ensure ifstream objects can throw exceptions:
    shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try
    {
      // open files
      shaderFile.open(_fileName);
      std::stringstream shaderStream;
      // read file's buffer contents into streams
      shaderStream << shaderFile.rdbuf();
      // close file handlers
      shaderFile.close();
      // convert stream into string
      shaderCode = shaderStream.str();
    }
    catch (std::ifstream::failure e)
    {
      std::cerr
        << "file:" << __FILE__ << std::endl
        << "func:" << "ShaderData" << std::endl
        << "ifstream error:" << "" << std::endl
        << "file:" << fileName << std::endl
        << std::endl << std::endl;
    }
    shaderSrc = shaderCode.c_str();

    // ���� ��� �� �������� �� w7
    /*
    std::ifstream shaderFile(fileName);
    if (!shaderFile)
    {
      std::cerr
        << "file:" << __FILE__ << std::endl
        << "func:" << "ShaderData" << std::endl
        << "ifstream error:" << "" << std::endl
        << "file:" << fileName << std::endl
        << std::endl << std::endl;
    }

    while (!shaderFile.eof())
      shaderSrc.push_back(shaderFile.get());

    shaderFile.close();
    */

    std::cout << "ShaderData++" << std::endl;
  }


  ShaderData::~ShaderData()
  {
    std::cout << "ShaderData--" << std::endl;
  }
}
