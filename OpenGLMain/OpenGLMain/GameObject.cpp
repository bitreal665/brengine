#include "GameObject.h"

#include <iostream>

#include "Transform.h"
#include "AxisMeshData.h"
#include "TextureData.h"
#include "ShaderData.h"
#include "Mesh.h"
#include "Shader.h"
#include "AbstractTexture.h"
#include "Material.h"
#include "Renderer.h"


namespace engine
{
  std::shared_ptr<AbstractMesh> GameObject::axisMesh;
  std::shared_ptr<Material> GameObject::axisMaterial;

  void GameObject::LoadDebuResources()
  {
    auto meshData = std::shared_ptr<MeshData>((MeshData *)new AxisMeshData());
    auto vShaderData = std::shared_ptr<ShaderData>(new ShaderData("Data/Shaders/pos_norm_color_uv.vert"));
    auto fShaderData = std::shared_ptr<ShaderData>(new ShaderData("Data/Shaders/color.frag"));

    axisMesh = std::shared_ptr<AbstractMesh>((AbstractMesh *)new Mesh(meshData));

    auto shader = std::shared_ptr<Shader>(new Shader(vShaderData, std::shared_ptr<ShaderData>(), fShaderData));
    shader->SetRenderPassName("forward");
    shader->SetOrder(0);

    axisMaterial = std::shared_ptr<Material>(new Material());
    axisMaterial->AddShader(shader);
  }

  void GameObject::UnloadDebuResources()
  {
    axisMesh.reset();
    axisMaterial.reset();
  }

  GameObject::GameObject(const std::string & _name) :
    name(_name)
  {
    AddComponent((Component *)new Transform());

    auto axisR = new Renderer(axisMaterial, axisMesh);
    axisR->SetLayer("debug");
    AddComponent((Component *)axisR);

    std::cout << "GameObject++" << std::endl;
  }

  GameObject::~GameObject()
  {
    std::cout << "GameObject--" << std::endl;
  }

  void GameObject::AddComponent(Component * component)
  {
    components.push_back(std::shared_ptr<Component>(component));
    component->SetGameObject(this);
  }

  void GameObject::RemoveComponent(Component * component)
  {
    for (auto it = components.begin(); it != components.end(); ++it)
    {
      if (it->get() == component)
      {
        components.erase(it);
        component->SetGameObject(nullptr);
        break;
      }
    }
  }

  void GameObject::GetComponents(std::vector<Component *> & vec) const
  {
    for (auto comp : components)
      vec.push_back(comp.get());
  }
}
