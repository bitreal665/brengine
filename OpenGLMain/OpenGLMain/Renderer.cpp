#include "Renderer.h"

#include <gtc/matrix_transform.hpp>

#include "GameObject.h"
#include "Transform.h"
#include "AxisMeshData.h"
#include "TextureData.h"
#include "ShaderData.h"
#include "Shader.h"


namespace engine
{
  DECLARE_COMPONENT(Renderer)

  Renderer::Renderer(std::shared_ptr<Material> _material, std::shared_ptr<engine::AbstractMesh> _mesh) :
    material(_material),
		mesh(_mesh),
    layer("opaque")
	{
	}

	Renderer::~Renderer()
	{
	}
	
	void Renderer::Render(RenderingContext & context) const
	{
    context.debugOutput += "\t" + GetGameObject()->GetName() + "\n";

		context.modelTransformationMatrix = GetGameObject()->FindComponent<Transform>()->BuildL2WMatrix();
    context.normalModelTransformationMatrix = glm::mat3x3(context.modelTransformationMatrix);
    context.normalModelTransformationMatrix = glm::transpose(glm::inverse(context.normalModelTransformationMatrix));

    material->Render(context);
    mesh->Render(context);
	}
}
