#pragma once

#include <string>

#define GLEW_STATIC
#include <GL/glew.h>


namespace engine
{
  class ShaderData
  {
  public:
    ShaderData(const std::string & _fileName);
    ~ShaderData();

    const std::string & GetFileName() const { return fileName; }
    const GLchar * GetShaderSrc() const { return shaderSrc.c_str(); }

  private:
    std::string fileName;
    std::basic_string<GLchar> shaderSrc;
  };
}

