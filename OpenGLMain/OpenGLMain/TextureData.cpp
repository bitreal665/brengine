#include "TextureData.h"

#include <iostream>

#include <SOIL.h>


namespace engine
{
  TextureData::TextureData(const std::string & fileName, int format) :
    format(format)
  {
    image = SOIL_load_image(fileName.c_str(), &width, &height, 0, SOIL_LOAD_AUTO);
    if (!image)
    {
      std::cerr
        << "file:" << __FILE__ << std::endl
        << "func:" << "TextureData" << std::endl
        << "SOIL error:" << SOIL_last_result() << std::endl
        << "file:" << fileName << std::endl
        << std::endl << std::endl;
    }

    std::cout << "TextureData++" << std::endl;
  }


  TextureData::~TextureData()
  {
    SOIL_free_image_data(image);

    std::cout << "TextureData--" << std::endl;
  }
}
