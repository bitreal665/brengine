#include "AxisMeshData.h"


namespace engine
{
  AxisMeshData::AxisMeshData()
  {
    // vertex
    // pos
    PushBackVertexAttribute(VertexAttribute::POS_IDX, 3);

    // uv1
    PushBackVertexAttribute(VertexAttribute::COLOR_IDX, 4);

    vertexCount = 6;

    // pos
    // uv
    GLfloat vertices[] = {
      0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
      1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,

      0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
      0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

      0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
    };
    for (int i = 0; i < vertexCount * 7; ++i)
      vertexData.push_back(vertices[i]);

    // index
    isIndexDataExist = false;

    // draw mode
    drawMode = GL_LINES;
  }


  AxisMeshData::~AxisMeshData()
  {
  }
}
