#include "Camera.h"


namespace engine
{
  DECLARE_COMPONENT(Camera)

	Camera::Camera(float fov, float w, float h, float near, float far) :
    BaseCamera(fov, w, h, near, far)
  {
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glEnable(GL_DEPTH_TEST);
  }

	Camera::~Camera()
	{
	}

	void Camera::Render(RenderingContext & context, const std::vector<std::shared_ptr<GameObject>> & objects) const
	{
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_TRUE);
    glStencilMask(0xFF);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    int SCR_WIDTH = 1000; // FIXME
    int SCR_HEIGHT = 600;
    int gBuffer = 2;

    glBindFramebuffer(GL_READ_FRAMEBUFFER, gBuffer);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); // write to default framebuffer
    glBlitFramebuffer(0, 0, SCR_WIDTH, SCR_HEIGHT, 0, 0, SCR_WIDTH, SCR_HEIGHT, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    BaseCamera::Render(context, objects);
	}
}
