#pragma once

#include <memory>
#include <string>
#include <vector>

#include "Component.h"

namespace engine
{
  class AbstractMesh;
  class Material;

  class GameObject
  {
  public:
    static void LoadDebuResources();
    static void UnloadDebuResources();

    GameObject(const std::string & _name);
    ~GameObject();

    const std::string & GetName() const { return name; }

    void AddComponent(Component * component);
    void RemoveComponent(Component * component);

    template<typename T> T * FindComponent() const
    {
      for (auto comp : components)
        if (comp->IsMyType(T::TYPE))
          return (T *)comp.get();
      return nullptr;
    }
    template<typename T> void FindComponents(std::vector<T *> & vec) const
    {
      for (auto comp : components)
        if (comp->IsMyType(T::TYPE))
          vec.push_back((T *)comp.get());
    }
    void GetComponents(std::vector<Component *> & vec) const;

  private:
    static std::shared_ptr<AbstractMesh> axisMesh;
    static std::shared_ptr<Material> axisMaterial;

    std::string name;
    std::vector<std::shared_ptr<Component>> components;
  };
}

