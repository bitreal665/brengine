#pragma once

#include "BaseCamera.h"


namespace engine
{
	class Camera : public BaseCamera
	{
    DEFINE_COMPONENT(Camera, BaseCamera)

	public:
    Camera(float fov, float w, float h, float near, float far);
		~Camera() override;

    void Render(RenderingContext & context, const std::vector<std::shared_ptr<GameObject>> & objects) const override;

	private:
		glm::mat4x4 projection;
	};
}


