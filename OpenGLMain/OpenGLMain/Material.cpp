#include "Material.h"

#include <iostream>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <gtc/type_ptr.hpp>

#include <gtc/matrix_transform.hpp>



const int MISC_IDX = 0;
const std::string MISC_NAME = "misc";
const int MISC_SIZE = sizeof(engine::Material::MiscInfo);
const int MISC_OFFSET = 256 * 0;// 0;

const int MATRIX_IDX = 1;
const std::string MATRIX_NAME = "matrices";
const int MATRIX_SIZE = sizeof(engine::Material::MatrixInfo);
const int MATRIX_OFFSET = 256 * 1;// MISC_OFFSET + MISC_SIZE;

const int AMB_LIGHT_IDX = 2;
const std::string AMB_LIGHT_NAME = "ambientLight";
const int AMB_LIGHT_SIZE = sizeof(engine::Material::AmbientLightInfo);
const int AMB_LIGHT_OFFSET = 256 * 2;// MATRIX_OFFSET + MATRIX_SIZE;

const int DIR_LIGHT_IDX = 3;
const std::string DIR_LIGHT_NAME = "dirLight";
const int DIR_LIGHT_SIZE = sizeof(engine::Material::DirLightInfo);
const int DIR_LIGHT_OFFSET = 256 * 3;// AMB_LIGHT_OFFSET + AMB_LIGHT_SIZE;

const int POINT_LIGHT_IDX = 4;
const std::string POINT_LIGHT_NAME = "pointLight";
const int POINT_LIGHT_SIZE = sizeof(engine::Material::PointLightInfo);
const int POINT_LIGHT_OFFSET = 256 * 4;// DIR_LIGHT_OFFSET + DIR_LIGHT_SIZE;

const int SPOT_LIGHT_IDX = 5;
const std::string SPOT_LIGHT_NAME = "spotLight";
const int SPOT_LIGHT_SIZE = sizeof(engine::Material::SpotLightInfo);
const int SPOT_LIGHT_OFFSET = 256 * 5;// POINT_LIGHT_SIZE + POINT_LIGHT_OFFSET;

constexpr int UBO_SIZE = SPOT_LIGHT_OFFSET + SPOT_LIGHT_SIZE;


namespace engine
{
  GLuint Material::UBO = 0;

  void Material::InitUniformBuffer()
  {
    // Now actually create the buffer
    glGenBuffers(1, &UBO);
    glBindBuffer(GL_UNIFORM_BUFFER, UBO);
    glBufferData(GL_UNIFORM_BUFFER, UBO_SIZE, nullptr, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferRange(GL_UNIFORM_BUFFER, MISC_IDX, UBO, MISC_OFFSET, MISC_SIZE);
    // FIXME: �� ������� ����� ����������� ������ glBindBufferRange ������������� error � GL_INVALID_VALUE 
    // �� �������� �������� (� ����� �� �������� � 4, 8, 16, 32, 64)
    // ������� ����� ��������, ��� ��� �������� � 256 ������ ���
    glBindBufferRange(GL_UNIFORM_BUFFER, MATRIX_IDX, UBO, MATRIX_OFFSET, MATRIX_SIZE);
    glBindBufferRange(GL_UNIFORM_BUFFER, AMB_LIGHT_IDX, UBO, AMB_LIGHT_OFFSET, AMB_LIGHT_SIZE);
    glBindBufferRange(GL_UNIFORM_BUFFER, DIR_LIGHT_IDX, UBO, DIR_LIGHT_OFFSET, DIR_LIGHT_SIZE);
    glBindBufferRange(GL_UNIFORM_BUFFER, POINT_LIGHT_IDX, UBO, POINT_LIGHT_OFFSET, POINT_LIGHT_SIZE);
    glBindBufferRange(GL_UNIFORM_BUFFER, SPOT_LIGHT_IDX, UBO, SPOT_LIGHT_OFFSET, SPOT_LIGHT_SIZE);
  }

  void Material::DeinitUniformBuffer()
  {
    glDeleteBuffers(1, &UBO);
  }

  void Material::PutMiscInfo(const MiscInfo & s)
  {
    glBindBuffer(GL_UNIFORM_BUFFER, Material::UBO);
    glBufferSubData(GL_UNIFORM_BUFFER, MISC_OFFSET, MISC_SIZE, &s);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
  }

  void Material::PutMatrixInfo(const MatrixInfo & s)
  {
    glBindBuffer(GL_UNIFORM_BUFFER, Material::UBO);
    glBufferSubData(GL_UNIFORM_BUFFER, MATRIX_OFFSET, MATRIX_SIZE, &s);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
  }

  void Material::PutAmbientLightInfo(const AmbientLightInfo & s)
  {
    glBindBuffer(GL_UNIFORM_BUFFER, Material::UBO);
    glBufferSubData(GL_UNIFORM_BUFFER, AMB_LIGHT_OFFSET, AMB_LIGHT_SIZE, &s);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
  }

  void Material::PutDirLightInfo(const DirLightInfo & s)
  {
    glBindBuffer(GL_UNIFORM_BUFFER, Material::UBO);
    glBufferSubData(GL_UNIFORM_BUFFER, DIR_LIGHT_OFFSET, DIR_LIGHT_SIZE, &s);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
  }

  void Material::PutPointLightInfo(const PointLightInfo & s)
  {
    glBindBuffer(GL_UNIFORM_BUFFER, Material::UBO);
    glBufferSubData(GL_UNIFORM_BUFFER, POINT_LIGHT_OFFSET, POINT_LIGHT_SIZE, &s);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
  }

  void Material::PutSpotLightInfo(const SpotLightInfo & s)
  {
    glBindBuffer(GL_UNIFORM_BUFFER, Material::UBO);
    glBufferSubData(GL_UNIFORM_BUFFER, SPOT_LIGHT_OFFSET, SPOT_LIGHT_SIZE, &s);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
  }

  // ==============

	Material::Material()
	{
    std::cout << "Material++" << std::endl;
	}

	Material::~Material()
	{
    std::cout << "Material--" << std::endl;
	}

  void Material::AddShader(std::shared_ptr<Shader> shader)
  {
    unsigned int miscBlockIdx = glGetUniformBlockIndex(shader->GetShaderProgram(), MISC_NAME.c_str());
    if (miscBlockIdx != GL_INVALID_INDEX)
      glUniformBlockBinding(shader->GetShaderProgram(), miscBlockIdx, MISC_IDX);

    unsigned int matrixBlockIdx = glGetUniformBlockIndex(shader->GetShaderProgram(), MATRIX_NAME.c_str());
    if (matrixBlockIdx != GL_INVALID_INDEX)
      glUniformBlockBinding(shader->GetShaderProgram(), matrixBlockIdx, MATRIX_IDX);

    unsigned int ambLightBlockIdx = glGetUniformBlockIndex(shader->GetShaderProgram(), AMB_LIGHT_NAME.c_str());
    if (ambLightBlockIdx != GL_INVALID_INDEX)
      glUniformBlockBinding(shader->GetShaderProgram(), ambLightBlockIdx, AMB_LIGHT_IDX);

    unsigned int dirLightBlockIdx = glGetUniformBlockIndex(shader->GetShaderProgram(), DIR_LIGHT_NAME.c_str());
    if (dirLightBlockIdx != GL_INVALID_INDEX)
      glUniformBlockBinding(shader->GetShaderProgram(), dirLightBlockIdx, DIR_LIGHT_IDX);

    unsigned int pointLightBlockIdx = glGetUniformBlockIndex(shader->GetShaderProgram(), POINT_LIGHT_NAME.c_str());
    if (pointLightBlockIdx != GL_INVALID_INDEX)
      glUniformBlockBinding(shader->GetShaderProgram(), pointLightBlockIdx, POINT_LIGHT_IDX);

    unsigned int spotLightBlockIdx = glGetUniformBlockIndex(shader->GetShaderProgram(), SPOT_LIGHT_NAME.c_str());
    if (spotLightBlockIdx != GL_INVALID_INDEX)
      glUniformBlockBinding(shader->GetShaderProgram(), spotLightBlockIdx, SPOT_LIGHT_IDX);

    shaders.push_back(shader);
  }

  bool Material::WillRender(const RenderingContext & context) const
  {
    for (auto sh : shaders)
    {
      if (sh->GetRenderPassName() != "" && sh->GetRenderPassName() != context.renderPassName)
        continue;
      return true;
    }
    return false;
  }

  int Material::GetOrder(const RenderingContext & context) const
  {
    for (auto sh : shaders)
    {
      if (sh->GetRenderPassName() != "" && sh->GetRenderPassName() != context.renderPassName)
        continue;
      return sh->GetOrder();
    }
    return 0;
  }

	void Material::Render(RenderingContext & context) const
	{
    for (auto sh : shaders)
    {
      if (sh->GetRenderPassName() != "" && sh->GetRenderPassName() != context.renderPassName)
        continue;

      // cull
      if (sh->cullFace)
        glEnable(GL_CULL_FACE);
      else
        glDisable(GL_CULL_FACE);
      glCullFace(sh->cullFaceMode);
      // z
      glDepthMask(sh->zWrite);
      glDepthFunc(sh->zFunc);
      // blend
      if (sh->blend)
        glEnable(GL_BLEND);
      else
        glDisable(GL_BLEND);
      glBlendFuncSeparate(sh->rgbSFactor, sh->rgbDFactor, sh->aSFactor, sh->aDFactor);
      glBlendEquationSeparate(sh->rgbBlendQuation, sh->aBlendQuation);
      // stencil
      if (sh->stencil)
        glEnable(GL_STENCIL_TEST);
      else
        glDisable(GL_STENCIL_TEST);
      glStencilFunc(sh->stencilFunc, sh->stencilRef, sh->stencilMask);
      glStencilOp(sh->stencilSFailOp, sh->stencilDFailOp, sh->stencilPassOp);

      // uniforms

      // use shader
      glUseProgram(sh->GetShaderProgram());

      // matrix
      UpdateUniform(sh, "model", context.modelTransformationMatrix);
      UpdateUniform(sh, "normalModel", context.normalModelTransformationMatrix);

      // texture
      int texIdx = 0;
      for (auto p : uTex)
      {
        GLint texULoc = glGetUniformLocation(sh->GetShaderProgram(), p.first.c_str());
        if (texULoc != -1)
        {
          glActiveTexture(GL_TEXTURE0 + texIdx);
          glBindTexture(p.second->GetTextureType(), p.second->GetTexture());
          glUniform1i(texULoc, texIdx);
          ++texIdx;
        }
      }

      //
      for (auto p : uVec4)
        UpdateUniform(sh, p.first, p.second);

      break;
    }
  }

  void Material::UpdateUniform(std::shared_ptr<Shader> shader, const std::string & uName, const glm::vec4 & v) const
  {
    GLint uLoc = glGetUniformLocation(shader->GetShaderProgram(), uName.c_str());
    if (uLoc != -1)
      glUniform4f(uLoc, v.x, v.y, v.z, v.w);
  }

  void Material::UpdateUniform(std::shared_ptr<Shader> shader, const std::string & uName, const glm::mat3x3 & v) const
  {
    GLint uLoc = glGetUniformLocation(shader->GetShaderProgram(), uName.c_str());
    if (uLoc != -1)
      glUniformMatrix3fv(uLoc, 1, GL_FALSE, glm::value_ptr(v));
  }

  void Material::UpdateUniform(std::shared_ptr<Shader> shader, const std::string & uName, const glm::mat4x4 & v) const
  {
    GLint uLoc = glGetUniformLocation(shader->GetShaderProgram(), uName.c_str());
    if (uLoc != -1)
      glUniformMatrix4fv(uLoc, 1, GL_FALSE, glm::value_ptr(v));
  }
}
