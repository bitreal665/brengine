#pragma once

#define GLEW_STATIC
#include <GL/glew.h>


namespace engine
{
  class AbstractTexture
  {
  public:
    virtual GLuint GetTexture() const = 0;
    virtual GLenum GetTextureType() const = 0;
  };
}

