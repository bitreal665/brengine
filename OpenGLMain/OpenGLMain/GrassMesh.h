#pragma once

#include "Mesh.h"


namespace engine
{
  class GrassMesh : public Mesh
  {
  public:
    GrassMesh(std::shared_ptr<MeshData> _meshData);
    ~GrassMesh() override;

  protected:
    void InternalRender(RenderingContext & renderingContext) const override;

  private:
    GLuint instanceVBO;
    int instanceCount;

    void GenerateInstanceMeshData(std::vector<GLfloat> & vertexData, std::vector<VertexAttribute> & vertextAttributes, int & vertexDataStride, int & instanceCount);
  };
}

