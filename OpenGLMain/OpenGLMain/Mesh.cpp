#include "Mesh.h"

#include <iostream>


namespace engine
{

  Mesh::Mesh(std::shared_ptr<MeshData> _meshData) :
    meshData(_meshData)
  {
    // vao start
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    // vbo
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    auto vertexData = _meshData->GetVertexData();
    // 0 - ��� �������
    // 1 - ������ (� ������)
    // 2 - ����
    // 4 - ������� ����������
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertexData.size(), &vertexData[0], GL_STATIC_DRAW);

    auto vertextAttributes = _meshData->GetVertextAttributes();
    for (auto & attr : vertextAttributes)
    {
      // 0 - ������ � �������
      // 1 - ����������� ��������� (���������� ���������)
      // 2 - ���
      // 3 - �������������?
      // 4 - ��� (� ������)
      // 5 - �������� (� ������)
      glVertexAttribPointer(attr.index, attr.size, GL_FLOAT, GL_FALSE, _meshData->GetVertexDataStrid(), (void *)attr.offset);
      glEnableVertexAttribArray(attr.index);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    vertexCount = _meshData->GetVertexCount();

    // ebo
    if (_meshData->IsIndexDataExist())
    {
      glGenBuffers(1, &EBO);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
      auto indexData = _meshData->GetIndexData();
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * _meshData->getIndexCount(), &indexData[0], GL_STATIC_DRAW);

      indexCount = _meshData->getIndexCount();
    }
    else
    {
      indexCount = 0;
    }

    // vao stop
    glBindVertexArray(0);

    drawMode = _meshData->GetDrawMode();

    std::cout << "Mesh++" << std::endl;
  }

  Mesh::~Mesh()
  {
    if (indexCount != 0)
      glDeleteBuffers(1, &EBO);
    glDeleteBuffers(1, &VBO);
    glDeleteVertexArrays(1, &VAO);

    std::cout << "Mesh--" << std::endl;
  }

  void Mesh::Render(RenderingContext & renderingContext) const
  {
    if (VAO == 0)
      return;

    glBindVertexArray(VAO);
    InternalRender(renderingContext);
    glBindVertexArray(0);
  }

  void Mesh::InternalRender(RenderingContext & renderingContext) const
  {
    if (indexCount > 0)
      glDrawElements(drawMode, indexCount, GL_UNSIGNED_INT, 0);
    else
      glDrawArrays(drawMode, 0, vertexCount);
  }

}
