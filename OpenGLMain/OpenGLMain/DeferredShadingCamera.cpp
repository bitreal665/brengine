#include "DeferredShadingCamera.h"

#include <iostream>

#include "RenderTexture.h"


namespace engine
{
  DECLARE_COMPONENT(DeferredShadingCamera)

  DeferredShadingCamera::DeferredShadingCamera(float fov, float w, float h, float near, float far) :
    BaseCamera(fov, w, h, near, far)
  {
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

    posTexture = std::shared_ptr<AbstractTexture>((AbstractTexture *)new RenderTexture(w, h, GL_RGB16F, GL_RGB, GL_FLOAT));
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, posTexture->GetTexture(), 0);

    normTexture = std::shared_ptr<AbstractTexture>((AbstractTexture *)new RenderTexture(w, h, GL_RGB16F, GL_RGB, GL_FLOAT));
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normTexture->GetTexture(), 0);

    colSpecTexture = std::shared_ptr<AbstractTexture>((AbstractTexture *)new RenderTexture(w, h, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE));
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, colSpecTexture->GetTexture(), 0);

    unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
    glDrawBuffers(3, attachments);

    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, w, h);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cerr
        << "file:" << __FILE__ << std::endl
        << "func:" << "RenderToTextureCamera" << std::endl
        << "fbo creating error:" << "" << std::endl
        << std::endl << std::endl;
    }

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glEnable(GL_DEPTH_TEST);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  DeferredShadingCamera::~DeferredShadingCamera()
  {
    glDeleteRenderbuffers(1, &rbo);
    glDeleteFramebuffers(1, &framebuffer);
  }

  void DeferredShadingCamera::Render(RenderingContext & context, const std::vector<std::shared_ptr<GameObject>> & objects) const
  {
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_TRUE);
    glStencilMask(0xFF);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    BaseCamera::Render(context, objects);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
}