#version 330 core

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_norm;
layout(location = 8) in vec2 v_uv;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

uniform mat4 model;
uniform mat3 normalModel;

out VS_OUT
{
    vec4 pos;
    vec3 normal;
    vec2 uv;
} vs_out;

void main()
{
	gl_Position = projection * view * model * vec4(v_position, 1.0f);

    vs_out.pos = model * vec4(v_position, 1.0f);
    vs_out.pos /= vs_out.pos.w;

    vs_out.normal = normalize(normalModel * v_norm);

	vs_out.uv = v_uv;
}