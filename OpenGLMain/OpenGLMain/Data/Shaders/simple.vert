#version 330 core

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNorm;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

uniform mat4 model;
uniform mat3 normalModel;

out VS_OUT
{
    vec4 col;
} vs_out;

void main()
{
	gl_Position = projection * view * model * vec4(inPos, 1.0f);

    vec3 w_light_dir = vec3(-1.0f, -1.0f, -1.0f);
    w_light_dir = normalize(w_light_dir);

    vec3 l_norm = vec3(inNorm);
    //vs_out.col = vec4(l_norm, 1.0f);

    vec3 w_norm = normalModel * l_norm;
    //vec3 w_norm = mat3(transpose(inverse(model))) * l_norm;
    w_norm = normalize(w_norm);
    //vs_out.col = vec4(w_norm, 1.0f);

    vs_out.col = vec4(1.0f, 1.0f, 1.0f, 1.0f) * (max(0.0f, dot(w_norm, -w_light_dir)) * 0.5f + 0.5f);
}