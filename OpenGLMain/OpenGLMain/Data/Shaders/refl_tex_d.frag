#version 330 core

uniform sampler2D tex0;
uniform samplerCube tex1;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

in VS_OUT
{
    vec3 pos;
    vec3 norm;
    vec4 color0;
    vec2 uv0;
} fs_in;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

void main()
{
    gPosition = fs_in.pos;
    gNormal = normalize(fs_in.norm);

    vec4 col0 = texture(tex0, fs_in.uv0);
    vec3 toPos = normalize(fs_in.pos - viewPos);
    vec3 norm = normalize(fs_in.norm);
    vec3 r = reflect(toPos, norm);
	vec4 col1 = texture(tex1, r);
    gAlbedoSpec.rgb = (col0 + col1 * (col0.r + col0.g + col0.b) * 0.15).rgb;
    
    gAlbedoSpec.a = 0.0;
}