#version 330 core

uniform samplerCube tex0;

in VS_OUT
{
    vec3 texCoord;
} fs_in;

out vec4 color;

void main()
{
	color = texture(tex0, fs_in.texCoord);
}