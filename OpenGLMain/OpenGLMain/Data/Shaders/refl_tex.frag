#version 330 core

uniform sampler2D tex0;
uniform samplerCube tex1;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

in VS_OUT
{
    vec3 pos;
    vec3 norm;
    vec4 color0;
    vec2 uv0;
} fs_in;

out vec4 color;

void main()
{
    vec4 col0 = texture(tex0, fs_in.uv0);
    vec3 toPos = normalize(fs_in.pos - viewPos);
    vec3 norm = normalize(fs_in.norm);
    vec3 r = reflect(toPos, norm);
	vec4 col1 = texture(tex1, r);
    color = col0 + col1 * (col0.r + col0.g + col0.b) * 0.15;
}