#version 330 core

layout(location = 0) in vec3 v_pos;
layout(location = 1) in vec3 v_norm;
layout(location = 4) in vec4 v_color0;
layout(location = 8) in vec2 v_uv0;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

uniform mat4 model;
uniform mat3 normalModel;

out VS_OUT
{
    vec3 pos;
    vec3 norm;
    vec4 color0;
    vec2 uv0;
} vs_out;

void main()
{
    vec4 w_pos = model * vec4(v_pos, 1.0f);
	gl_Position = projection * view * w_pos;
    vs_out.pos = w_pos.xyz / w_pos.w;
    vs_out.norm = normalize(normalModel * v_norm);
	vs_out.color0 = v_color0;
	vs_out.uv0 = v_uv0;
}