#version 330 core

layout (std140) uniform ambientLight
{
    vec4 ambientLight_color;
};

layout (std140) uniform dirLight
{
    vec3 dirLight_dir;
    vec4 dirLight_color;
};

layout (std140) uniform pointLight
{
	vec3 pointLight_pos;
    vec4 pointLight_color;
    float pointLight_constant;
    float pointLight_linear;
    float pointLight_quadratic;
};

layout (std140) uniform spotLight
{
      vec3 spotLight_pos;
      vec3 spotLight_dir;
      vec4 spotLight_color;
      float spotLight_cutOff;
      float spotLight_outerCutOff;
      float spotLight_constant;
      float spotLight_linear;
      float spotLight_quadratic;
};

uniform sampler2D tex0; // gPosition
uniform sampler2D tex1;  // gNormal
uniform sampler2D tex2;  // gAlbedoSpec

in VS_OUT
{
    vec2 uv;
} fs_in;

out vec4 color;

vec4 CalcAmb()
{
	return ambientLight_color;
}

vec4 CalcDir(vec3 norm)
{
	return dirLight_color * max(dot(dirLight_dir, -norm), 0.0);
}

vec4 CalcPoint(vec3 pos, vec3 norm)
{
	vec3 lightDir = normalize(pointLight_pos - pos);
	float diff = max(dot(norm, lightDir), 0.0);
	float distance = length(pointLight_pos - pos);
    float attenuation = 1.0 / (pointLight_constant + pointLight_linear * distance + pointLight_quadratic * (distance * distance));  
	vec4 diffuse = pointLight_color * diff;
	return attenuation * diffuse;
}

vec4 CalcSpot(vec3 pos, vec3 norm)
{
	vec3 lightDir = normalize(spotLight_pos - pos);
	float diff = max(dot(norm, lightDir), 0.0);
	float distance = length(spotLight_pos - pos);
    float attenuation = 1.0 / (spotLight_constant + spotLight_linear * distance + spotLight_quadratic * (distance * distance)); 

    float theta = dot(lightDir, normalize(-spotLight_dir)); 
    float epsilon = spotLight_cutOff - spotLight_outerCutOff;
    float intensity = clamp((theta - spotLight_outerCutOff) / epsilon, 0.0, 1.0); 

	vec4 diffuse = spotLight_color * diff;
	return attenuation * intensity * diffuse;
}

void main()
{
    vec3 pos = texture(tex0, fs_in.uv).rgb;
    vec3 normal = texture(tex1, fs_in.uv).rgb;
    vec4 diffuse = vec4(texture(tex2, fs_in.uv).rgb, 1.0);
    float specular = texture(tex2, fs_in.uv).a;

	vec4 resultColor;
	resultColor += CalcAmb();
	resultColor += CalcDir(normal);
	resultColor += CalcPoint(pos, normal);
	resultColor += CalcSpot(pos, normal);
	resultColor.a = 1.0;
	color = resultColor * diffuse;
}

