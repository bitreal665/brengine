#version 330 core

uniform sampler2D ourTexture;

in VS_OUT
{
    vec2 texCoord;
} fs_in;

out vec4 color;

void main()
{
	color = texture(ourTexture, fs_in.texCoord);
}