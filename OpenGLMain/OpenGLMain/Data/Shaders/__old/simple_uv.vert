#version 330 core

layout(location = 0) in vec3 position;
layout(location = 8) in vec2 uv;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
};

uniform mat4 model;

out VS_OUT
{
    vec2 texCoord;
} vs_out;

void main()
{
	gl_Position = projection * view * model * vec4(position, 1.0f);
	vs_out.texCoord = uv;
}