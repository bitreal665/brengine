#version 330 core

layout(location = 0) in vec3 inPos;
layout(location = 4) in vec4 inCol;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
};

uniform mat4 model;

out VS_OUT
{
    vec4 col;
} vs_out;

void main()
{
	gl_Position = projection * view * model * vec4(inPos, 1.0f);
	vs_out.col = inCol;
}