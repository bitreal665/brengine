#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS_OUT
{
    vec2 texCoord;
} gs_in[];

out VS_OUT
{
    vec2 texCoord;
} gs_out;

void main() {
    gl_Position = gl_in[0].gl_Position;
    gs_out.texCoord = gs_in[0].texCoord;
    EmitVertex();
    gl_Position = gl_in[1].gl_Position;
    gs_out.texCoord = gs_in[1].texCoord;
    EmitVertex();
    gl_Position = gl_in[2].gl_Position;
    gs_out.texCoord = gs_in[2].texCoord;
    EmitVertex();
    EndPrimitive();
}  