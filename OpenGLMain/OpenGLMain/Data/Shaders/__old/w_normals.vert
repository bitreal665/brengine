#version 330 core

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNorm;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
};

uniform mat4 model;
uniform mat3 normalModel;

out VS_OUT
{
    vec3 normal;
    vec4 pos;
} vs_out;

void main()
{
	gl_Position = projection * view * model * vec4(inPos, 1.0f);  

    vec3 l_norm = inNorm * model[0][0];
    vs_out.normal = normalize(normalModel * l_norm);
    vec4 w_pos = model * vec4(inPos, 1.0f);
    vs_out.pos = w_pos / w_pos.w;
}