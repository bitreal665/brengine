#version 330 core

layout (std140) uniform ambientLight
{
    vec4 ambientLight_color;
};

layout (std140) uniform dirLight
{
    vec3 dirLight_dir;
    vec4 dirLight_color;
};

layout (std140) uniform pointLight
{
	vec4 pointLight_pos;
    vec4 pointLight_color;
    float pointLight_constant;
    float pointLight_linear;
    float pointLight_quadratic;
};

layout (std140) uniform spotLight
{
      vec4 spotLight_pos;
      vec3 spotLight_dir;
      vec4 spotLight_color;
      float spotLight_cutOff;
      float spotLight_outerCutOff;
      float spotLight_constant;
      float spotLight_linear;
      float spotLight_quadratic;
};

in VS_OUT
{
    vec3 normal;
	vec4 pos;
} fs_in;

out vec4 color;

vec4 CalcAmb()
{
	return ambientLight_color;
}

vec4 CalcDir()
{
	return dirLight_color * max(dot(dirLight_dir, -fs_in.normal), 0.0);
}

vec4 CalcPoint()
{
	vec3 lightDir = normalize(pointLight_pos.xyz - fs_in.pos.xyz);
	float diff = max(dot(fs_in.normal, lightDir), 0.0);
	float distance = length(pointLight_pos - fs_in.pos);
    float attenuation = 1.0 / (pointLight_constant + pointLight_linear * distance + pointLight_quadratic * (distance * distance));  
	vec4 diffuse = pointLight_color * diff;
	return attenuation * diffuse;
}

vec4 CalcSpot()
{
	vec3 lightDir = normalize(spotLight_pos.xyz - fs_in.pos.xyz);
	float diff = max(dot(fs_in.normal, lightDir), 0.0);
	float distance = length(spotLight_pos - fs_in.pos);
    float attenuation = 1.0 / (spotLight_constant + spotLight_linear * distance + spotLight_quadratic * (distance * distance)); 

    float theta = dot(lightDir, normalize(-spotLight_dir)); 
    float epsilon = spotLight_cutOff - spotLight_outerCutOff;
    float intensity = clamp((theta - spotLight_outerCutOff) / epsilon, 0.0, 1.0); 

	vec4 diffuse = spotLight_color * diff;
	return attenuation * intensity * diffuse;
}

void main()
{
	vec4 resultColor;
	resultColor += CalcAmb();
	resultColor += CalcDir();
	resultColor += CalcPoint();
	resultColor += CalcSpot();
	resultColor.a = 1.0;
	color = resultColor;
}