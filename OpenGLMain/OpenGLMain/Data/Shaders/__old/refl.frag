#version 330 core

uniform sampler2D tex0;
uniform samplerCube tex1;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec4 viewPos;
};

in VS_OUT
{
    vec4 pos;
    vec3 normal;
    vec2 uv;
} fs_in;

out vec4 color;

void main()
{
    vec4 col0 = texture(tex0, fs_in.uv);


    vec4 toPos = fs_in.pos - viewPos;
    toPos.xyz = normalize(toPos.xyz);

    vec3 norm = fs_in.normal;
    norm = normalize(norm);

    vec3 r = reflect(toPos.xyz, norm.xyz);

	vec4 col1 = texture(tex1, r);

    color = col0 + col1 * (col0.r + col0.g + col0.b) * 0.15;
}