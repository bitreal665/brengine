#version 330 core

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNorm;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
};

uniform mat4 model;
uniform mat3 normalModel;

out VS_OUT
{
    vec3 normal;
} vs_out;

void main()
{
	gl_Position = projection * view * model * vec4(inPos, 1.0f);  

    vec3 l_norm = inNorm * model[0][0];
    vec3 w_norm = normalize(normalModel * l_norm);
    vs_out.normal = vec3(projection * view * vec4(w_norm, 0.0));
}