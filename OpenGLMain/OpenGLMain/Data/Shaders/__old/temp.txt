// FORWARD
// + normals
// + axis
// + lightMarker
// + cube1, cube2, tree, grass floor + mirror (plane)
// + refl (floor)
// + grass
// skybox
// deferredPost
// deferredDebug

// DEFERRED
// + cube1, cube2, tree, grass floor + mirror (plane)
// + refl (floor)
// + grass


// COMMON

#define VER 
#version 330 core

#define MISC
layout (std140) uniform misc
{
    float misc_time;
};

#define MATICES
layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

#define AMB_LIGHT
layout (std140) uniform ambientLight
{
    vec4 ambientLight_color;
};

#define DIR_LIGHT
layout (std140) uniform dirLight
{
    vec3 dirLight_dir;
    vec4 dirLight_color;
};

#define POINT_LIGHT
layout (std140) uniform pointLight
{
	vec4 pointLight_pos;
    vec4 pointLight_color;
    float pointLight_constant;
    float pointLight_linear;
    float pointLight_quadratic;
};

#define SPOT_LIGHT
layout (std140) uniform spotLight
{
      vec4 spotLight_pos;
      vec3 spotLight_dir;
      vec4 spotLight_color;
      float spotLight_cutOff;
      float spotLight_outerCutOff;
      float spotLight_constant;
      float spotLight_linear;
      float spotLight_quadratic;
};

uniform mat4 model


// VERT

#define V_POS layout(location = 0) in vec3 v_pos;
#define V_NORM layout(location = 1) in vec3 v_norm;
#define V_COL0 layout(location = 4) in vec4 v_col0;
#define V_UV0 layout(location = 8) in vec2 v_uv0;