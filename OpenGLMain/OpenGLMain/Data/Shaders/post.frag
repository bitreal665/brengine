#version 330 core

uniform sampler2D tex0;

in VS_OUT
{
    vec2 uv;
} fs_in;

out vec4 color;

void main()
{
	color = vec4(texture(tex0, fs_in.uv).rgb, 1.0);// * vec4(1.0, 0.6, 0.6, 1.0);
}