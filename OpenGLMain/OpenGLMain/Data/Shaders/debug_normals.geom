#version 330 core

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

in VS_OUT
{
    vec3 pos;
    vec3 norm;
    vec4 color0;
    vec2 uv0;
} gs_in[];

const float MAGNITUDE = 0.3;

void GenerateLine(int index)
{
    gl_Position = gl_in[index].gl_Position;
    EmitVertex();
    vec4 w_pos = vec4(gs_in[index].pos + gs_in[index].norm * MAGNITUDE, 1.0);
    gl_Position = projection * view * w_pos;
    EmitVertex();
    EndPrimitive();
}

void main()
{
    GenerateLine(0);
    GenerateLine(1);
    GenerateLine(2);
}