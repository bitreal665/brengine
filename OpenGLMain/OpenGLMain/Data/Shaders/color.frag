#version 330 core

in VS_OUT
{
    vec3 pos;
    vec3 norm;
    vec4 color0;
    vec2 uv0;
} fs_in;

out vec4 color;

void main()
{
    color = fs_in.color0;
}