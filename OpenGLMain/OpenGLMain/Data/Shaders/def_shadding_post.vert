#version 330 core

layout(location = 0) in vec3 v_position;
layout(location = 8) in vec2 v_uv;

uniform mat4 model;

out VS_OUT
{
    vec2 uv;
} vs_out;

void main()
{
	gl_Position = model * vec4(v_position, 1.0f);
	vs_out.uv = v_uv;
}