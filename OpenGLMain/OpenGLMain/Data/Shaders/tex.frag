#version 330 core

uniform sampler2D tex0;

in VS_OUT
{
    vec3 pos;
    vec3 norm;
    vec4 color0;
    vec2 uv0;
} fs_in;

out vec4 color;

void main()
{
    color = texture(tex0, fs_in.uv0);
}