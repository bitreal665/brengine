#version 330 core

layout(location = 0) in vec3 position;

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

out VS_OUT
{
    vec3 texCoord;
} vs_out;

void main()
{
    mat4 _view = view;
    // игнорируем перемещение и проекцию (?)
    _view[3][0] = 0.0f;
    _view[3][1] = 0.0f;
    _view[3][2] = 0.0f;
    _view[3][3] = 1.0f;
    _view[0][3] = 0.0f;
    _view[1][3] = 0.0f;
    _view[2][3] = 0.0f;
    //_view /= _view[3][3]
	vs_out.texCoord = position;
	vec4 pos = projection * _view * vec4(position, 1.0f);
    // после деления на w мы получим 1 в z (максимально возможный z)
    gl_Position = pos.xyww;
}