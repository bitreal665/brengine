#version 330 core

layout(location = 0) in vec3 v_pos;
layout(location = 1) in vec3 v_norm;
layout(location = 4) in vec4 v_color0;
layout(location = 8) in vec2 v_uv0;
layout(location = 12) in vec4 v_trans;   // dx dy rot(0y) scale(xyz)

layout (std140) uniform misc
{
    float misc_time;
};

layout (std140) uniform matrices
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

uniform mat4 model;
uniform mat3 normalModel;

out VS_OUT
{
    vec3 pos;
    vec3 norm;
    vec4 color0;
    vec2 uv0;
} vs_out;

float CalcROx()
{
    vec4 w_pos = model * vec4(v_pos, 1.0);
    w_pos /= w_pos.w;
    w_pos += vec4(v_trans.x, 0.0, v_trans.y, 0.0);

    // wave_speed = f(time, x, y)
    float wave_speed = 25.0f * sin(misc_time * 0.15) * 0.25 + 1.0 + (sin(w_pos.x * 0.2) + cos(w_pos.z * 0.2)) * 0.25 + 0.5;
    w_pos.x += misc_time * wave_speed;
    w_pos.z += misc_time * wave_speed;

    // 
    float wave_size = 0.1;
    float a =  sin(w_pos.x * wave_size) + cos(w_pos.z * wave_size); // [-2 ... 2]
    a = a * 0.25 + 0.5; // [0 ... 1]

    return a * 3.14 * 0.15; // [0 ... pi * 0.15] (rad)
}

void main()
{
    vec3 l_pos = v_pos;

    //
    l_pos.y += 0.5;
    // rot (ox)
    // [col][row]
    float ox = CalcROx();
    mat3 r_ox;
    r_ox[0][0] = 1.0;
    r_ox[1][1] = cos(ox);
    r_ox[1][2] = sin(ox);
    r_ox[2][1] = -sin(ox);
    r_ox[2][2] = cos(ox);
    mat3 r_oy;
    float oy = v_trans.w;
    r_oy[0][0] = cos(oy);
    r_oy[0][2] = -sin(oy);
    r_oy[1][1] = 1.0;
    r_oy[2][0] = sin(oy);
    r_oy[2][2] = cos(oy);
    l_pos = r_oy * r_ox * l_pos;
    // scale
    l_pos *= v_trans.z;
    //
    vec4 w_pos = model * vec4(l_pos, 1.0f);
    w_pos /= w_pos.w;
    w_pos += vec4(v_trans.x, 0.0, v_trans.y, 0.0);
    //
	gl_Position = projection * view * w_pos;

    vs_out.pos = w_pos.xyz;

    mat3 _normalModel = transpose(inverse(r_oy * r_ox));
    vs_out.norm = normalize(_normalModel * v_norm);
	vs_out.color0 = v_color0;
	vs_out.uv0 = v_uv0;
}