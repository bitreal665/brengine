#version 330 core

uniform sampler2D tex0;

in VS_OUT
{
    vec3 pos;
    vec3 norm;
    vec4 color0;
    vec2 uv0;
} fs_in;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

void main()
{
    gPosition = fs_in.pos;
    gNormal = normalize(fs_in.norm);
    gAlbedoSpec.rgb = texture(tex0, fs_in.uv0).rgb;
    gAlbedoSpec.a = 0.0;
}