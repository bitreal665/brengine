#version 330 core

uniform sampler2D tex0;

in VS_OUT
{
    vec4 pos;
    vec3 normal;
    vec2 uv;
} fs_in;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

void main()
{    
    gPosition = fs_in.pos.xyz;

    gNormal = normalize(fs_in.normal);

    gAlbedoSpec.rgb = texture(tex0, fs_in.uv).rgb;
    gAlbedoSpec.a = 0.0;
}

