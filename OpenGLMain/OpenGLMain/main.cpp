#include <iostream>
#include <string>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "CubeMeshData.h"
#include "AxisMeshData.h"
#include "PlaneMeshData.h"
#include "AssimpMeshData.h"
#include "TextureData.h"
#include "ShaderData.h"

#include "Mesh.h"
#include "GrassMesh.h"
#include "Shader.h"
#include "Texture.h"
#include "CubeMapTexture.h"
#include "Material.h"

#include "GameObject.h"

#include "Transform.h"
#include "Renderer.h"
#include "Camera.h"
#include "RenderToTextureCamera.h"
#include "DeferredShadingCamera.h"
#include "CameraController.h"
#include "AmbientLight.h"
#include "DirLight.h"
#include "PointLight.h"
#include "SpotLight.h"
#include "RotationAnimation.h"


using namespace std;
using namespace engine;


constexpr int WIDTH = 1000;
constexpr int HEIGHT = 600;


void KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

bool InitGLFW(GLFWwindow ** windowPtrPtr)
{
	if (glfwInit() != GLFW_TRUE)
	{
		cerr << "glfwInit error" << endl;
		return false;
	}
  glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	auto window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", nullptr, nullptr);
	if (window == nullptr)
	{
		cout << "glfwCreateWindow return nullptr" << endl;
		glfwTerminate();
		return false;
	}
	glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, KeyCallback);

	*windowPtrPtr = window;

	return true;
}

void DeinitGLFW()
{
	glfwTerminate();
}

bool InitGLEW()
{
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		cout << "glewInit error" << endl;
		return false;
	}

	return true;
}

bool InitGL(GLFWwindow * window)
{
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	glViewport(0, 0, width, height);

  // TODO: ��������� � ��������
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glLineWidth(3);

  glEnable(GL_MULTISAMPLE);

	return true;
}

void DeinitScene()
{
  BaseLight::UnloadDebuResources();
  GameObject::UnloadDebuResources();
  Material::DeinitUniformBuffer();
}

bool InitScene(vector<shared_ptr<GameObject>> & gameObjects)
{
  Material::InitUniformBuffer();
  GameObject::LoadDebuResources();
  BaseLight::LoadDebuResources();

  // CAMERA

  auto rCameraGo = shared_ptr<GameObject>(new GameObject("rCamera"));
  rCameraGo->FindComponent<Transform>()->SetPosition(glm::vec3(-1.0f, 1.0f, 5.0f));
  auto rCamera = new RenderToTextureCamera(45, WIDTH, HEIGHT, 0.1f, 1000);
  rCamera->SetRenderPassName("forward");
  rCamera->AddLayer("opaque");
  rCamera->AddLayer("transporent");
  rCamera->SetOrder(0);
  rCameraGo->AddComponent((Component *)rCamera);
  gameObjects.push_back(rCameraGo);

  auto cameraGo = shared_ptr<GameObject>(new GameObject("camera"));
  cameraGo->FindComponent<Transform>()->SetPosition(glm::vec3(-1.0f, 1.0f, 5.0f));
  auto camera = new DeferredShadingCamera(45, WIDTH, HEIGHT, 0.1f, 1000);
  camera->SetRenderPassName("deferred");
  camera->AddLayer("opaque");
  camera->SetOrder(1);
  cameraGo->AddComponent((Component *)camera);
  gameObjects.push_back(cameraGo);

  auto dCamera = new Camera(45, WIDTH, HEIGHT, 0.1f, 1000);
  dCamera->SetRenderPassName("forward");
  dCamera->AddLayer("resultQuad");
  dCamera->AddLayer("debug");
  dCamera->AddLayer("skybox");
  dCamera->AddLayer("transporent");
  dCamera->SetOrder(2);
  cameraGo->AddComponent((Component *)dCamera);

  cameraGo->AddComponent((Component *)new CameraController());

	// RESOURCE

  // mesh
  auto cubeMeshData = shared_ptr<MeshData>((MeshData *)new CubeMeshData());
  auto planeMeshData = shared_ptr<MeshData>((MeshData *)new PlaneMeshData());
  vector<shared_ptr<MeshData>> treeMeshData;
  AssimpMeshData::ParseMeshes("Data/Lowpoly_tree_sample.obj", treeMeshData);

  // shader
  auto pos_norm_color_uv__vShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/pos_norm_color_uv.vert"));
  auto grass_vShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/grass.vert"));
  auto debug_normals__gShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/debug_normals.geom"));
  auto tex__fShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/tex.frag"));
  auto tex_d__fShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/tex_d.frag"));
  auto refl_tex__fShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/refl_tex.frag"));
  auto refl_tex_d__fShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/refl_tex_d.frag"));
  auto ucolor__fShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/ucolor.frag"));
  auto ucolor_d__fShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/ucolor_d.frag"));
  auto grass_fShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/grass.frag"));
  auto grass_d_fShData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/grass_d.frag"));

  auto post_vShaderData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/post.vert"));
  auto post_fShaderData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/post.frag"));

  auto def_shadding_post_vShaderData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/def_shadding_post.vert"));
  auto def_shadding_post_fShaderData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/def_shadding_post.frag"));

  auto skybox_vShaderData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/skybox.vert"));
  auto skybox_fShaderData = shared_ptr<ShaderData>(new ShaderData("Data/Shaders/skybox.frag"));

  // texture
  auto textureData = shared_ptr<TextureData>(new TextureData("Data/test512.jpg", GL_RGB));
  auto textureData2 = shared_ptr<TextureData>(new TextureData("Data/wood.png", GL_RGB));
  auto sbPosX = shared_ptr<TextureData>(new TextureData("Data/Yokohama3/posx.jpg", GL_RGB));
  auto sbNegX = shared_ptr<TextureData>(new TextureData("Data/Yokohama3/negx.jpg", GL_RGB));
  auto sbPosY = shared_ptr<TextureData>(new TextureData("Data/Yokohama3/posy.jpg", GL_RGB));
  auto sbNegY = shared_ptr<TextureData>(new TextureData("Data/Yokohama3/negy.jpg", GL_RGB));
  auto sbPosZ = shared_ptr<TextureData>(new TextureData("Data/Yokohama3/posz.jpg", GL_RGB));
  auto sbNegZ = shared_ptr<TextureData>(new TextureData("Data/Yokohama3/negz.jpg", GL_RGB));
  auto grasTextureData = shared_ptr<TextureData>(new TextureData("Data/Grass2_mini.png", GL_RGBA));
  auto grasFloorTextureData = shared_ptr<TextureData>(new TextureData("Data/unnamed.png", GL_RGBA));

  // RESOURCE 2

  // mesh
  auto cubeMesh = shared_ptr<AbstractMesh>((AbstractMesh *)new Mesh(cubeMeshData));
  auto planeMesh = shared_ptr<AbstractMesh>((AbstractMesh *)new Mesh(planeMeshData));
  vector<shared_ptr<AbstractMesh>> treeMesh;
  for (auto md : treeMeshData)
  {
    auto m = shared_ptr<AbstractMesh>((AbstractMesh *)new Mesh(md));
    treeMesh.push_back(m);
  }
  auto grassMesh = shared_ptr<AbstractMesh>((AbstractMesh *)new GrassMesh(planeMeshData));

  // shader
  auto planeShader = shared_ptr<Shader>(new Shader(pos_norm_color_uv__vShData, shared_ptr<ShaderData>(), tex__fShData));
  planeShader->SetRenderPassName("forward");
  planeShader->SetOrder(0);
  auto plane_d_Shader = shared_ptr<Shader>(new Shader(pos_norm_color_uv__vShData, shared_ptr<ShaderData>(), tex_d__fShData));
  plane_d_Shader->SetRenderPassName("deferred");
  plane_d_Shader->SetOrder(0);

  auto floorShader = shared_ptr<Shader>(new Shader(pos_norm_color_uv__vShData, shared_ptr<ShaderData>(), refl_tex__fShData));
  floorShader->SetRenderPassName("forward");
  floorShader->SetOrder(0);
  auto floor_d_Shader = shared_ptr<Shader>(new Shader(pos_norm_color_uv__vShData, shared_ptr<ShaderData>(), refl_tex_d__fShData));
  floor_d_Shader->SetRenderPassName("deferred");
  floor_d_Shader->SetOrder(0);

  auto treeShader = shared_ptr<Shader>(new Shader(pos_norm_color_uv__vShData, shared_ptr<ShaderData>(), ucolor__fShData));
  treeShader->SetRenderPassName("forward");
  treeShader->SetOrder(0);
  auto tree_d_Shader = shared_ptr<Shader>(new Shader(pos_norm_color_uv__vShData, shared_ptr<ShaderData>(), ucolor_d__fShData));
  tree_d_Shader->SetRenderPassName("deferred");
  tree_d_Shader->SetOrder(0);

  auto grassShader = shared_ptr<Shader>(new Shader(grass_vShData, shared_ptr<ShaderData>(), grass_fShData));
  grassShader->SetRenderPassName("forward");
  grassShader->SetOrder(0);
  grassShader->cullFace = false;
  auto grass_d_Shader = shared_ptr<Shader>(new Shader(grass_vShData, shared_ptr<ShaderData>(), grass_d_fShData));
  grass_d_Shader->SetRenderPassName("deferred");
  grass_d_Shader->SetOrder(0);
  grass_d_Shader->cullFace = false;

  auto sbShader = shared_ptr<Shader>(new Shader(skybox_vShaderData, shared_ptr<ShaderData>(), skybox_fShaderData));
  sbShader->SetRenderPassName("forward");
  sbShader->SetOrder(1);
  sbShader->cullFaceMode = GL_FRONT;
  sbShader->zWrite = GL_FALSE;
  sbShader->zFunc = GL_LEQUAL;

  auto postShader = shared_ptr<Shader>(new Shader(post_vShaderData, shared_ptr<ShaderData>(), post_fShaderData));
  postShader->SetRenderPassName("forward");
  postShader->SetOrder(0);

  auto defShadingPostShader = shared_ptr<Shader>(new Shader(def_shadding_post_vShaderData, shared_ptr<ShaderData>(), def_shadding_post_fShaderData));
  defShadingPostShader->SetRenderPassName("forward");
  defShadingPostShader->SetOrder(-1);
  defShadingPostShader->zWrite = GL_FALSE;
  defShadingPostShader->zFunc = GL_ALWAYS;

  auto normalsDebugShader = std::shared_ptr<Shader>(new Shader(pos_norm_color_uv__vShData, debug_normals__gShData, ucolor__fShData));
  normalsDebugShader->SetRenderPassName("forward");
  normalsDebugShader->SetOrder(0);

  // texture
  auto texture = shared_ptr<AbstractTexture>((AbstractTexture *)new Texture(textureData, GL_CLAMP_TO_EDGE, GL_RGB));
  auto texture2 = shared_ptr<AbstractTexture>((AbstractTexture *)new Texture(textureData2, GL_CLAMP_TO_EDGE, GL_RGB));
  auto sbTexture = shared_ptr<AbstractTexture>((AbstractTexture *)new CubeMapTexture(sbPosX, sbNegX, sbPosY, sbNegY, sbPosZ, sbNegZ));
  auto grassTexture = shared_ptr<AbstractTexture>((AbstractTexture *)new Texture(grasTextureData, GL_CLAMP_TO_EDGE, GL_RGBA));
  auto grassFloorTexture = shared_ptr<AbstractTexture>((AbstractTexture *)new Texture(grasFloorTextureData, GL_REPEAT, GL_RGBA));

  // material
  auto floorMat = shared_ptr<Material>(new Material());
  floorMat->AddShader(floorShader);
  floorMat->AddShader(floor_d_Shader);
  floorMat->UpdateUTex("tex0", texture2);
  floorMat->UpdateUTex("tex1", sbTexture);

  auto cubeMat = shared_ptr<Material>(new Material());
  cubeMat->AddShader(planeShader);
  cubeMat->AddShader(plane_d_Shader);
  cubeMat->UpdateUTex("tex0", texture);

  auto planeMat = shared_ptr<Material>(new Material());
  planeMat->AddShader(planeShader);
  planeMat->AddShader(plane_d_Shader);
  planeMat->UpdateUTex("tex0", rCamera->GetTexture());

  auto treeMat = shared_ptr<Material>(new Material());
  treeMat->AddShader(treeShader);
  treeMat->AddShader(tree_d_Shader);
  treeMat->UpdateUVec4("col", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));

  auto grassMat = shared_ptr<Material>(new Material());
  grassMat->AddShader(grassShader);
  grassMat->AddShader(grass_d_Shader);
  grassMat->UpdateUTex("tex0", grassTexture);

  auto grassFloorMat = shared_ptr<Material>(new Material());
  grassFloorMat->AddShader(planeShader);
  grassFloorMat->AddShader(plane_d_Shader);
  grassFloorMat->UpdateUTex("tex0", grassFloorTexture);

  auto sbMat = shared_ptr<Material>(new Material());
  sbMat->AddShader(sbShader);
  sbMat->UpdateUTex("tex0", sbTexture);

  auto defShadingPostMat = shared_ptr<Material>(new Material());
  defShadingPostMat->AddShader(defShadingPostShader);
  defShadingPostMat->UpdateUTex("tex0", camera->GetPosTexture());
  defShadingPostMat->UpdateUTex("tex1", camera->GetNormTexture());
  defShadingPostMat->UpdateUTex("tex2", camera->GetColSpecTexture());

  auto defShadingDebugPosMat = shared_ptr<Material>(new Material());
  defShadingDebugPosMat->AddShader(postShader);
  defShadingDebugPosMat->UpdateUTex("tex0", camera->GetPosTexture());

  auto defShadingDebugNormMat = shared_ptr<Material>(new Material());
  defShadingDebugNormMat->AddShader(postShader);
  defShadingDebugNormMat->UpdateUTex("tex0", camera->GetNormTexture());

  auto defShadingDebugColSpecMat = shared_ptr<Material>(new Material());
  defShadingDebugColSpecMat->AddShader(postShader);
  defShadingDebugColSpecMat->UpdateUTex("tex0", camera->GetColSpecTexture());

  auto normalsDebugMaterial = std::shared_ptr<Material>(new Material());
  normalsDebugMaterial->AddShader(normalsDebugShader);
  normalsDebugMaterial->UpdateUVec4("col", glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));

	// COMPONENT

  auto cube1Go = shared_ptr<GameObject>(new GameObject("cube1"));
  cube1Go->FindComponent<Transform>()->SetPosition(glm::vec3(0.0f, 0.8f, 0.0f));
  auto cube1R = new Renderer(cubeMat, cubeMesh);
  cube1R->SetLayer("opaque");
  cube1Go->AddComponent((Component *)cube1R);
  auto cube1Anim = new RotationAnimation();
  cube1Anim->speed = glm::vec3(30.0f, 20.0f, 10.0f);
  cube1Go->AddComponent((Component *)cube1Anim);
  gameObjects.push_back(cube1Go);

  auto cube2Go = shared_ptr<GameObject>(new GameObject("cube2"));
  cube2Go->FindComponent<Transform>()->SetScale(glm::vec3(1.5f, 1.3f, 1.5f));
  cube2Go->FindComponent<Transform>()->SetPosition(glm::vec3(2.0f, 2.0f, 3.0f));
  auto cube2R = new Renderer(cubeMat, cubeMesh);
  cube2R->SetLayer("opaque");
  cube2Go->AddComponent((Component *)cube2R);
  gameObjects.push_back(cube2Go);

  auto axisGo = shared_ptr<GameObject>(new GameObject("axis"));
  axisGo->FindComponent<Transform>()->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
  axisGo->FindComponent<Transform>()->SetScale(glm::vec3(10.0f, 10.0f, 10.0f));
  gameObjects.push_back(axisGo);

  auto planeGo = shared_ptr<GameObject>(new GameObject("plane"));
  planeGo->FindComponent<Transform>()->SetPosition(glm::vec3(8.0f, 6.0f, -11.0f));
  planeGo->FindComponent<Transform>()->SetScale(glm::vec3(8.0f, 6.0f, 1.0f));
  planeGo->FindComponent<Transform>()->SetRotation(glm::vec3(30.0f, -30.0f, 0.0f));
  auto planeR = new Renderer(planeMat, planeMesh);
  planeR->SetLayer("opaque");
  planeGo->AddComponent((Component *)planeR);
  gameObjects.push_back(planeGo);

  for (auto m : treeMesh)
  {
    auto treeGo = shared_ptr<GameObject>(new GameObject("tree"));
    treeGo->FindComponent<Transform>()->SetPosition(glm::vec3(-5.0f, 0.0f, -5.0f));
    treeGo->FindComponent<Transform>()->SetScale(glm::vec3(0.3f, 0.3f, 0.3f));
    auto treeR = new Renderer(treeMat, m);
    treeR->SetLayer("opaque");
    treeGo->AddComponent((Component *)treeR);
    gameObjects.push_back(treeGo);

    auto treeDebugGo = shared_ptr<GameObject>(new GameObject("treeDebugGo"));
    treeDebugGo->FindComponent<Transform>()->SetPosition(treeGo->FindComponent<Transform>()->GetPosition());
    treeDebugGo->FindComponent<Transform>()->SetScale(treeGo->FindComponent<Transform>()->GetScale());
    treeDebugGo->FindComponent<Transform>()->SetRotation(treeGo->FindComponent<Transform>()->GetRotation());
    auto treeDebugR = new Renderer(normalsDebugMaterial, m);
    treeDebugR->SetLayer("debug");
    treeDebugGo->AddComponent((Component *)treeDebugR);
    gameObjects.push_back(treeDebugGo);
  }

  auto flooGo = shared_ptr<GameObject>(new GameObject("floor"));
  flooGo->FindComponent<Transform>()->SetScale(glm::vec3(15.0f, 15.0f, 1.0f));
  flooGo->FindComponent<Transform>()->SetRotation(glm::vec3(-90.0f, 0.0f, 0.0f));
  auto floorR = new Renderer(floorMat, planeMesh);
  floorR->SetLayer("opaque");
  flooGo->AddComponent((Component *)floorR);
  gameObjects.push_back(flooGo);

  auto grassGo = shared_ptr<GameObject>(new GameObject("grass"));
  grassGo->FindComponent<Transform>()->SetPosition(glm::vec3(35.0f, 2.0f, 35.0f));
  grassGo->FindComponent<Transform>()->SetScale(glm::vec3(1.0f, 1.0f, 1.0f));
  auto grass = new Renderer(grassMat, grassMesh);
  grass->SetLayer("opaque");
  grassGo->AddComponent((Component *)grass);
  gameObjects.push_back(grassGo);

  auto grassFloorGo = shared_ptr<GameObject>(new GameObject("grassFloor"));
  grassFloorGo->FindComponent<Transform>()->SetScale(glm::vec3(60.0f, 60.0f, 1.0f));
  grassFloorGo->FindComponent<Transform>()->SetRotation(glm::vec3(-90.0f, 0.0f, 0.0f));
  grassFloorGo->FindComponent<Transform>()->SetPosition(glm::vec3(35.0f, 2.0f, 35.0f));
  auto grassFloorR = new Renderer(grassFloorMat, planeMesh);
  grassFloorR->SetLayer("opaque");
  grassFloorGo->AddComponent((Component *)grassFloorR);
  gameObjects.push_back(grassFloorGo);

  auto sbGo = shared_ptr<GameObject>(new GameObject("skybox"));
  auto sb = new Renderer(sbMat, cubeMesh);
  sb->SetLayer("skybox");
  sbGo->AddComponent((Component *)sb);
  gameObjects.push_back(sbGo);

  auto ambLightGo = shared_ptr<GameObject>(new GameObject("ambLight"));
  ambLightGo->FindComponent<Transform>()->SetPosition(glm::vec3(0.0f, 10.0f, 0.0f));
  auto ambLight = new AmbientLight();
  ambLight->color = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
  ambLightGo->AddComponent((Component *)ambLight);
  gameObjects.push_back(ambLightGo);

  auto directLightGo = shared_ptr<GameObject>(new GameObject("directLight"));
  directLightGo->FindComponent<Transform>()->SetPosition(glm::vec3(-5.0f, 10.0f, -5.0f));
  directLightGo->FindComponent<Transform>()->SetRotation(glm::vec3(15.0f, 0.0f, 0.0f));
  auto dirLight = new DirLight();
  dirLight->color = glm::vec4(0.6f, 0.1f, 0.1f, 1.0f);
  directLightGo->AddComponent((Component *)dirLight);
  auto dirLightAnim = new RotationAnimation();
  dirLightAnim->speed = glm::vec3(0.0f, -15.0f, 0.0f);
  directLightGo->AddComponent((Component *)dirLightAnim);
  gameObjects.push_back(directLightGo);

  auto pointLightGo = shared_ptr<GameObject>(new GameObject("pointLight"));
  pointLightGo->FindComponent<Transform>()->SetPosition(glm::vec3(-2.0f, 5.0f, -4.0f));
  auto pointLight = new PointLight();
  pointLight->color = glm::vec4(0.1f, 0.9f, 0.1f, 1.0f);
  pointLight->constant = 1.0f;
  pointLight->linear = 0.22f;
  pointLight->quadratic = 0.20f;
  pointLightGo->AddComponent((Component *)pointLight);
  gameObjects.push_back(pointLightGo);

  auto spotLightGo = shared_ptr<GameObject>(new GameObject("spotLight"));
  spotLightGo->FindComponent<Transform>()->SetPosition(glm::vec3(-9.0f, 4.0f, -9.0f));
  spotLightGo->FindComponent<Transform>()->SetRotation(glm::vec3(15.0f, -135.0f, 0.0f));
  auto spotLight = new SpotLight();
  spotLight->color = glm::vec4(0.1f, 0.1f, 0.9f, 1.0f);
  spotLight->cutOff = glm::cos(glm::radians(12.5f));
  spotLight->outerCutOff = glm::cos(glm::radians(14.5f));
  spotLight->constant = 1.0f;
  spotLight->linear = 0.045f;
  spotLight->quadratic = 0.0075f;
  spotLightGo->AddComponent((Component *)spotLight);
  gameObjects.push_back(spotLightGo);

  auto posQuadGo = shared_ptr<GameObject>(new GameObject("posQuad"));
  posQuadGo->FindComponent<Transform>()->SetScale(glm::vec3(0.6f, 0.6f, 0.6f));
  posQuadGo->FindComponent<Transform>()->SetPosition(glm::vec3(0.67f, 0.66f, -1.0f));
  auto posQuad = new Renderer(defShadingDebugPosMat, planeMesh);
  posQuad->SetLayer("resultQuad");
  posQuadGo->AddComponent((Component *)posQuad);
  gameObjects.push_back(posQuadGo);
  
  auto normQuadGo = shared_ptr<GameObject>(new GameObject("normQuad"));
  normQuadGo->FindComponent<Transform>()->SetScale(glm::vec3(0.6f, 0.6f, 0.6f));
  normQuadGo->FindComponent<Transform>()->SetPosition(glm::vec3(0.67f, 0.0f, -1.0f));
  auto normQuad = new Renderer(defShadingDebugNormMat, planeMesh);
  normQuad->SetLayer("resultQuad");
  normQuadGo->AddComponent((Component *)normQuad);
  gameObjects.push_back(normQuadGo);

  auto colSpecQuadGo = shared_ptr<GameObject>(new GameObject("colSpecQuad"));
  colSpecQuadGo->FindComponent<Transform>()->SetScale(glm::vec3(0.6f, 0.6f, 0.6f));
  colSpecQuadGo->FindComponent<Transform>()->SetPosition(glm::vec3(0.67f, -0.66f, -1.0f));
  auto colSpecQuad = new Renderer(defShadingDebugColSpecMat, planeMesh);
  colSpecQuad->SetLayer("resultQuad");
  colSpecQuadGo->AddComponent((Component *)colSpecQuad);
  gameObjects.push_back(colSpecQuadGo);

  auto resultQuadGo = shared_ptr<GameObject>(new GameObject("resultQuad"));
  resultQuadGo->FindComponent<Transform>()->SetScale(glm::vec3(2.0f, 2.0f, 2.0f));
  resultQuadGo->FindComponent<Transform>()->SetPosition(glm::vec3(0.0f, 0.0f, -1.0f));
  auto resultQuad = new Renderer(defShadingPostMat, planeMesh);
  resultQuad->SetLayer("resultQuad");
  resultQuadGo->AddComponent((Component *)resultQuad);
  gameObjects.push_back(resultQuadGo);
  
	return true;
}

int main()
{
	GLFWwindow * window;

  if (!InitGLFW(&window))
  {
    system("pause");
    return -1;
  }

  vector<shared_ptr<GameObject>> gameObjects;

	if (!InitGLEW()
		|| !InitGL(window)
		|| !InitScene(gameObjects))
	{
    DeinitGLFW();
    system("pause");
		return -1;
	}

	GLfloat lastFrame = 0.0f;

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		GLfloat currentFrame = glfwGetTime();

		RenderingContext context;
		context.window = window;
		context.deltaTime = currentFrame - lastFrame;
    context.time = currentFrame;

    // update
    for (auto go : gameObjects)
    {
      vector<Component *> vec;
      go->GetComponents(vec);
      for (auto c : vec)
        c->Update(context);
    }

    // befor render
    for (auto go : gameObjects)
    {
      vector<Component *> vec;
      go->GetComponents(vec);
      for (auto c : vec)
        c->BeforRender(context);
    }

    //
    Material::MiscInfo s1;
    s1.time = context.time;
    Material::PutMiscInfo(s1);

    // cameras
    vector<BaseCamera *> cameras;
    for (auto go : gameObjects)
      go->FindComponents(cameras);
    sort(cameras.begin(), cameras.end(), [&](BaseCamera * el1, BaseCamera * el2) { return el1->GetOrder() < el2->GetOrder(); });

    // render
    for (auto c : cameras)
      c->Render(context, gameObjects);

		lastFrame = currentFrame;

		glfwSwapBuffers(window);

    //cout << context.debugOutput << endl;
    //break;
	}

  gameObjects.clear();

  DeinitScene();
	DeinitGLFW();
	system("pause");
	return 0;
}
