#pragma once

#include <vector>
#include <unordered_set>

#include "Component.h"
#include "Renderer.h"
#include "RenderingContext.h"


namespace engine
{
  class BaseCamera : public Component
  {
    DEFINE_COMPONENT(BaseCamera, Component)

  public:
    BaseCamera(float fov, float w, float h, float near, float far);
    ~BaseCamera() override;

    void AddLayer(const std::string & layer) { layers.insert(layer); }

    int GetOrder() const { return order; }
    void SetOrder(int _order) { order = _order; }

    const std::string & GetRenderPassName() const { return renderPassName; }
    void SetRenderPassName(const std::string & _renderPassName) { renderPassName = _renderPassName; }

    virtual void Render(RenderingContext & context, const std::vector<std::shared_ptr<GameObject>> & objects) const;

  private:
    glm::mat4x4 projection;
    std::unordered_set<std::string> layers;
    int order;
    std::string renderPassName;
  };
}


