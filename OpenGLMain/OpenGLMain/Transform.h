#pragma once

#include <glm.hpp>
#include "Component.h"


namespace engine
{
	class Transform : Component
	{
    DEFINE_COMPONENT(Transform, Component)

	public:
		Transform();
		~Transform() override;

		const glm::vec3 & GetPosition() const { return pos; }
		const glm::vec3 & GetScale() const { return scale; }
		const glm::vec3 & GetRotation() const { return rot; }

		glm::mat4 BuildL2WMatrix() const;

		void SetPosition(const glm::vec3 & _pos) { pos = _pos; }
		void SetScale(const glm::vec3 & _scale) { scale = _scale; }
		void SetRotation(const glm::vec3 & _rotation) { rot = _rotation; }

	private:
		glm::vec3 pos;
		glm::vec3 scale;
		glm::vec3 rot;
	};
}
