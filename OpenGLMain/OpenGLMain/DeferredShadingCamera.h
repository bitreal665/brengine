#pragma once

#include "BaseCamera.h"

namespace engine
{
  class DeferredShadingCamera : public BaseCamera
  {
    DEFINE_COMPONENT(DeferredShadingCamera, BaseCamera)

  public:
    DeferredShadingCamera(float fov, float w, float h, float near, float far);
    ~DeferredShadingCamera() override;

    void Render(RenderingContext & context, const std::vector<std::shared_ptr<GameObject>> & objects) const override;

    std::shared_ptr<AbstractTexture> GetPosTexture() const { return posTexture; }
    std::shared_ptr<AbstractTexture> GetNormTexture() const { return normTexture; }
    std::shared_ptr<AbstractTexture> GetColSpecTexture() const { return colSpecTexture; }

  private:
    GLuint framebuffer;
    GLuint rbo;
    std::shared_ptr<AbstractTexture> posTexture;
    std::shared_ptr<AbstractTexture> normTexture;
    std::shared_ptr<AbstractTexture> colSpecTexture;
  };
}
