#pragma once

#include "BaseLight.h"

namespace engine
{
  class SpotLight : public BaseLight
  {
    DEFINE_COMPONENT(SpotLight, BaseLight)

  public:
    float cutOff;
    float outerCutOff;
    float constant;
    float linear;
    float quadratic;
    float theta;
    float epsilon;

    SpotLight();
    ~SpotLight() override;

    void BeforRender(const RenderingContext & context) override;
  };
}

