#pragma once

#include <memory>

#include "Component.h"
#include "RenderingContext.h"

namespace engine
{
  class CameraController : Component
  {
    DEFINE_COMPONENT(CameraController, Component)

  public:
    CameraController();
    ~CameraController() override;

    void Update(const RenderingContext & context) override;

  private:
    double prevXMousePos, prevYMousePos;

    void UpdateRotation(const RenderingContext & context);
    void UpdatePosition(const RenderingContext & context);
  };
}

