#include "PlaneMeshData.h"


namespace engine
{
  PlaneMeshData::PlaneMeshData()
  {
    // vertex
    // pos
    PushBackVertexAttribute(VertexAttribute::POS_IDX, 3);

    // norm
    PushBackVertexAttribute(VertexAttribute::NORM_IDX, 3);

    // uv1
    PushBackVertexAttribute(VertexAttribute::UV_IDX, 2);

    vertexCount = 6;

    // pos
    // norm
    // uv
    GLfloat vertices[] = {
      -0.5f, -0.5f, 0,  0.0, 0.0, 1.0,  0.0f, 0.0f,
      0.5f, -0.5f, 0,   0.0, 0.0, 1.0,  1.0f, 0.0f,
      0.5f,  0.5f, 0,   0.0, 0.0, 1.0,  1.0f, 1.0f,
      0.5f,  0.5f, 0,   0.0, 0.0, 1.0,  1.0f, 1.0f,
      -0.5f,  0.5f, 0,  0.0, 0.0, 1.0,  0.0f, 1.0f,
      -0.5f, -0.5f, 0,  0.0, 0.0, 1.0,  0.0f, 0.0f,
    };
    for (int i = 0; i < (vertexCount * 8); ++i)
      vertexData.push_back(vertices[i]);

    // index
    isIndexDataExist = false;

    // draw mode
    drawMode = GL_TRIANGLES;
  }


  PlaneMeshData::~PlaneMeshData()
  {
  }
}
