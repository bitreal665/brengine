#pragma once

#include "BaseLight.h"

namespace engine
{
  class PointLight : public BaseLight
  {
    DEFINE_COMPONENT(PointLight, BaseLight)

  public:
    float constant;
    float linear;
    float quadratic;

    PointLight();
    ~PointLight() override;

    void BeforRender(const RenderingContext & context) override;
  };
}
