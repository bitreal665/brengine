#include "Transform.h"

#include <gtc/matrix_transform.hpp>


namespace engine
{
  DECLARE_COMPONENT(Transform)

	Transform::Transform()
	{
		pos = glm::vec3(0.0f, 0.0f, 0.0f);
		scale = glm::vec3(1.0f, 1.0f, 1.0f);
		rot = glm::vec3(0.0f, 0.0f, 0.0f);
	}


	Transform::~Transform()
	{
	}


	glm::mat4 Transform::BuildL2WMatrix() const
	{
		glm::mat4 model;
		model = glm::translate(model, pos);
		model = glm::rotate(model, rot.z, glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::rotate(model, rot.y, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, rot.x, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::scale(model, scale);
		return model;
	}
}
