#include "AmbientLight.h"

#include "Material.h"
#include "GameObject.h"
#include "Transform.h"


namespace engine
{
  DECLARE_COMPONENT(AmbientLight)

  AmbientLight::AmbientLight()
  {
  }

  AmbientLight::~AmbientLight()
  {
  }

  void AmbientLight::BeforRender(const RenderingContext & context)
  {
    Material::AmbientLightInfo s;
    s.color[0] = color.x;
    s.color[1] = color.y;
    s.color[2] = color.z;
    s.color[3] = color.w;
    Material::PutAmbientLightInfo(s);
  }
}