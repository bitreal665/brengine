#pragma once

#include "AbstractTexture.h"

namespace engine
{
  class RenderTexture : AbstractTexture
  {
  public:
    RenderTexture(int w, int h, GLint internalFormat, GLenum foramt, GLenum type);
    ~RenderTexture();

    GLuint GetTexture() const override { return texture; };
    GLuint GetTextureType() const override { return GL_TEXTURE_2D; };

  private:
    GLuint texture;
  };
}

