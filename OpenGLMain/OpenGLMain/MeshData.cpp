#include "MeshData.h"

#include <iostream>


namespace engine
{
  MeshData::MeshData() :
    isIndexDataExist(false),
    vertexCount(0),
    vertexDataStrid(0),
    drawMode(GL_TRIANGLES)
  {
    std::cout << "MeshData++" << std::endl;
  }

  MeshData::~MeshData()
  {
    std::cout << "MeshData--" << std::endl;
  }

  const std::vector<GLfloat> & MeshData::GetVertexData() const
  {
    return vertexData;
  }

  const std::vector<VertexAttribute> & MeshData::GetVertextAttributes() const
  {
    return vertexAttributes;
  }

  int MeshData::GetVertexCount() const
  {
    return vertexCount;
  }

  int MeshData::GetVertexDataStrid() const
  {
    return vertexDataStrid;
  }

  bool MeshData::IsIndexDataExist() const
  {
    return isIndexDataExist;
  }

  const std::vector<GLuint> & MeshData::GetIndexData() const
  {
    return indexData;
  }

  int MeshData::getIndexCount() const
  {
    return indexData.size();
  }

  void MeshData::PushBackVertexAttribute(int index, int size)
  {
    vertexAttributes.push_back(VertexAttribute(index, size, vertexDataStrid));
    vertexDataStrid += size * sizeof(GLfloat);
  }

  GLenum MeshData::GetDrawMode() const
  {
    return drawMode;
  }
}
