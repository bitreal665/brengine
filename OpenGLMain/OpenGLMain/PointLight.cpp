#include "PointLight.h"

#include "Material.h"
#include "GameObject.h"
#include "Transform.h"


namespace engine
{
  DECLARE_COMPONENT(PointLight)

  PointLight::PointLight()
  {
  }

  PointLight::~PointLight()
  {
  }

  void PointLight::BeforRender(const RenderingContext & context)
  {
    Material::PointLightInfo s;
    auto w_pos = GetGameObject()->FindComponent<Transform>()->GetPosition();
    s.position[0] = w_pos.x;
    s.position[1] = w_pos.y;
    s.position[2] = w_pos.z;
    s.color[0] = color.x;
    s.color[1] = color.y;
    s.color[2] = color.z;
    s.color[3] = color.w;
    s.constant = constant;
    s.linear = linear;
    s.quadratic = quadratic;
    Material::PutPointLightInfo(s);
  }
}