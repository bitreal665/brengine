#include "BaseCamera.h"

#include <algorithm>

#include "GameObject.h"
#include "Transform.h"
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>


namespace engine
{
  DECLARE_COMPONENT(BaseCamera)

  BaseCamera::BaseCamera(float fov, float w, float h, float near, float far) :
    order(0)
  {
    projection = glm::perspective(fov, w / h, near, far);
  }

  BaseCamera::~BaseCamera()
  {
  }

  void BaseCamera::Render(RenderingContext & context, const std::vector<std::shared_ptr<GameObject>> & objects) const
  {
    context.debugOutput += GetGameObject()->GetName() + " (" + GetRenderPassName() + ") ";
    context.debugOutput += "+";
    for (auto & l : layers)
      context.debugOutput += " " + l;
    context.debugOutput += "\n";

    // 
    Material::MatrixInfo s2;
    s2.projection = projection;
    s2.view = glm::inverse(GetGameObject()->FindComponent<Transform>()->BuildL2WMatrix());
    auto w_pos = GetGameObject()->FindComponent<Transform>()->GetPosition();
    s2.viewPos[0] = w_pos.x;
    s2.viewPos[1] = w_pos.y;
    s2.viewPos[2] = w_pos.z;
    Material::PutMatrixInfo(s2);

    context.renderPassName = renderPassName;

    // filter
    std::vector<Renderer *> renderers;
    for (auto go : objects)
    {
      std::vector<Renderer *> _renderers;
      go->FindComponents<Renderer>(_renderers);
      for (auto r : _renderers)
      {
        if (r->GetMaterial()->WillRender(context))
          renderers.push_back(r);
      }
    }
    // sort
    sort(renderers.begin(), renderers.end(), [&](Renderer * el1, Renderer * el2) { return el1->GetMaterial()->GetOrder(context) < el2->GetMaterial()->GetOrder(context); });
    // render
    for (auto r_ptr : renderers)
      if (layers.find(r_ptr->GetLayer()) != layers.end())
        r_ptr->Render(context);
  }
}
