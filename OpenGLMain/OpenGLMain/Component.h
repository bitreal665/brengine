#pragma once

#include <string>

#include "RenderingContext.h"

#define DEFINE_COMPONENT(T, BaseT) \
public:\
static const std::string TYPE;\
bool IsMyType(const std::string & type) const override { return T::TYPE == type || BaseT::IsMyType(type); };\

#define DECLARE_COMPONENT(T) \
const std::string T::TYPE = #T;

namespace engine
{
  class GameObject;

  class Component
  {
  public:
    static const std::string Component::TYPE;

    Component();
    virtual ~Component();

    GameObject * GetGameObject() const;
    virtual void SetGameObject(GameObject * _gameObject);

    virtual bool IsMyType(const std::string & type) const;

    virtual void Update(const RenderingContext & context);
    virtual void BeforRender(const RenderingContext & context);

  private:
    GameObject * gameObject;
  };
}

