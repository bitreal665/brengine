#include "CameraController.h"

#include "GameObject.h"
#include "Transform.h"


namespace engine
{
  DECLARE_COMPONENT(CameraController)

  CameraController::CameraController() :
    prevXMousePos(0),
    prevYMousePos(0)
  {
  }

  CameraController::~CameraController()
  {
  }

  void CameraController::Update(const RenderingContext & context)
  {
    Component::Update(context);
    UpdateRotation(context);
    UpdatePosition(context);
  }

  void CameraController::UpdateRotation(const RenderingContext & context)
  {
    float angleMouseSpeed = 50.0f;
    float angleKeyboardSpeed = 70.0f;
    glm::vec3 rot = GetGameObject()->FindComponent<Transform>()->GetRotation();

    double xMousePos, yMousePos;
    glfwGetCursorPos(context.window, &xMousePos, &yMousePos);

    if (glfwGetKey(context.window, GLFW_KEY_SPACE))
    {
      glfwSetInputMode(context.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

      glm::vec3 mouseDeltaPos(prevYMousePos - yMousePos, prevXMousePos - xMousePos, 0.0f);
      rot += mouseDeltaPos * context.deltaTime * angleMouseSpeed;
    }
    else
    {
      glfwSetInputMode(context.window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

      // x
      if (glfwGetKey(context.window, GLFW_KEY_UP) == GLFW_PRESS)
        rot.x += context.deltaTime * angleKeyboardSpeed;
      if (glfwGetKey(context.window, GLFW_KEY_DOWN) == GLFW_PRESS)
        rot.x -= context.deltaTime * angleKeyboardSpeed;
      // y
      if (glfwGetKey(context.window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        rot.y -= context.deltaTime * angleKeyboardSpeed;
      if (glfwGetKey(context.window, GLFW_KEY_LEFT) == GLFW_PRESS)
        rot.y += context.deltaTime * angleKeyboardSpeed;
    }

    prevXMousePos = xMousePos;
    prevYMousePos = yMousePos;
    //
    GetGameObject()->FindComponent<Transform>()->SetRotation(rot);
  }

  void CameraController::UpdatePosition(const RenderingContext & context)
  {
    float moveSpeed = 10.0f;
    glm::vec3 dWPos(0.0f, 0.0f, 0.0f);
    glm::vec4 dLPos(0.0f, 0.0f, 0.0f, 0.0f);
    // x
    if (glfwGetKey(context.window, GLFW_KEY_D) == GLFW_PRESS)
      dLPos.x = context.deltaTime * moveSpeed;
    if (glfwGetKey(context.window, GLFW_KEY_A) == GLFW_PRESS)
      dLPos.x = -context.deltaTime * moveSpeed;
    // y
    if (glfwGetKey(context.window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
      dWPos.y = context.deltaTime * moveSpeed;
    if (glfwGetKey(context.window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
      dWPos.y = -context.deltaTime * moveSpeed;
    // z
    if (glfwGetKey(context.window, GLFW_KEY_W) == GLFW_PRESS)
      dLPos.z = -context.deltaTime * moveSpeed;
    if (glfwGetKey(context.window, GLFW_KEY_S) == GLFW_PRESS)
      dLPos.z = context.deltaTime * moveSpeed;
    //
    auto tr = GetGameObject()->FindComponent<Transform>();
    auto l2wmatrix = tr->BuildL2WMatrix();
    dWPos += glm::vec3(l2wmatrix * dLPos);
    tr->SetPosition(tr->GetPosition() + dWPos);
  }
}
