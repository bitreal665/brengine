#include "Texture.h"

#include <iostream>


namespace engine
{
	Texture::Texture(std::shared_ptr<TextureData> _textureData, GLint clampMode, GLenum format) :
    textureData(_textureData)
	{
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    // ������������� ��������� ���������� � �������������� (�� ������� ��������)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clampMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clampMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // ��������� � ���������� ��������
    glTexImage2D(
      GL_TEXTURE_2D, 
      0, 
      _textureData->GetFormat(),
      _textureData->GetWidth(), 
      _textureData->GetHeight(), 
      0, 
      format,
      GL_UNSIGNED_BYTE, 
      _textureData->GetTextureBytes());
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    std::cout << "Texture++" << std::endl;
	}

	Texture::~Texture()
	{
    glDeleteTextures(1, &texture);

    std::cout << "Texture--" << std::endl;
	}
}
