#include "Component.h"

#include <iostream>


namespace engine
{
  const std::string Component::TYPE = "Component";

  Component::Component()
  {
    std::cout << "Component++" << std::endl;
  }

  Component::~Component()
  {
    std::cout << "Component--" << std::endl;
  }

  GameObject * Component::GetGameObject() const
  {
    return gameObject;
  }

  void Component::SetGameObject(GameObject * _gameObject)
  {
    gameObject = _gameObject;
  }

  bool Component::IsMyType(const std::string & type) const
  {
    return Component::TYPE == type;
  }

  void Component::Update(const RenderingContext & context)
  {
  }

  void Component::BeforRender(const RenderingContext & context)
  {
  }
}
