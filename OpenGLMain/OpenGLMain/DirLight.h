#pragma once

#include "BaseLight.h"

namespace engine
{
  class DirLight : public BaseLight
  {
    DEFINE_COMPONENT(DirLight, BaseLight)

  public:
    DirLight();
    ~DirLight() override;

    void BeforRender(const RenderingContext & context) override;
  };
}

