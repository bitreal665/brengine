#pragma once

#include "BaseCamera.h"

namespace engine
{
  class RenderToTextureCamera : public BaseCamera
  {
    DEFINE_COMPONENT(RenderToTextureCamera, BaseCamera)

  public:
    RenderToTextureCamera(float fov, float w, float h, float near, float far);
    ~RenderToTextureCamera() override;

    void Render(RenderingContext & context, const std::vector<std::shared_ptr<GameObject>> & objects) const override;

    std::shared_ptr<AbstractTexture> GetTexture() const;

  private:
    GLuint framebuffer;
    GLuint rbo;
    std::shared_ptr<AbstractTexture> texture;
  };
}
