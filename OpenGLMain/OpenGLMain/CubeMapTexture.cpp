#include "CubeMapTexture.h"

#include <iostream>


namespace engine
{
  CubeMapTexture::CubeMapTexture(
    std::shared_ptr<TextureData> _posXTextureData, 
    std::shared_ptr<TextureData> _negXTextureData, 
    std::shared_ptr<TextureData> _posYTextureData, 
    std::shared_ptr<TextureData> _negYTextureData, 
    std::shared_ptr<TextureData> _posZTextureData, 
    std::shared_ptr<TextureData> _negZTextureData) :
    posXTextureData(_posXTextureData),
    negXTextureData(_negXTextureData),
    posYTextureData(_posYTextureData),
    negYTextureData(_negYTextureData),
    posZTextureData(_posZTextureData),
    negZTextureData(_negZTextureData)
  {
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, _posXTextureData->GetWidth(), _posXTextureData->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, _posXTextureData->GetTextureBytes());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, _negXTextureData->GetWidth(), _negXTextureData->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, _negXTextureData->GetTextureBytes());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, _posYTextureData->GetWidth(), _posYTextureData->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, _posYTextureData->GetTextureBytes());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, _negYTextureData->GetWidth(), _negYTextureData->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, _negYTextureData->GetTextureBytes());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, _posZTextureData->GetWidth(), _posZTextureData->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, _posZTextureData->GetTextureBytes());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, _negZTextureData->GetWidth(), _negZTextureData->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, _negZTextureData->GetTextureBytes());
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    std::cout << "CubeMapTexture++" << std::endl;
  }

  CubeMapTexture::~CubeMapTexture()
  {
    glDeleteTextures(1, &texture);

    std::cout << "CubeMapTexture--" << std::endl;
  }
}
