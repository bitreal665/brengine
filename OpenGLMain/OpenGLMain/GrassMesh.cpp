#include "GrassMesh.h"

#include <stdlib.h>


namespace engine
{

  GrassMesh::GrassMesh(std::shared_ptr<MeshData> _meshData) :
    Mesh(_meshData)
  {
    glBindVertexArray(VAO);


    std::vector<GLfloat> vertexData;
    std::vector<VertexAttribute> vertextAttributes;
    int vertexDataStride = 0;
    GenerateInstanceMeshData(vertexData, vertextAttributes, vertexDataStride, instanceCount);

    glGenBuffers(1, &instanceVBO);
    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertexData.size(), &vertexData[0], GL_STATIC_DRAW);

    for (auto & attr : vertextAttributes)
    {
      glVertexAttribPointer(attr.index, attr.size, GL_FLOAT, GL_FALSE, vertexDataStride, (void*)attr.offset);
      glEnableVertexAttribArray(attr.index);

      glVertexAttribDivisor(attr.index, 1);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);


    glBindVertexArray(0);
  }

  GrassMesh::~GrassMesh()
  {
    glDeleteBuffers(1, &instanceVBO);
  }

  void GrassMesh::InternalRender(RenderingContext & renderingContext) const
  {
    if (indexCount > 0)
      glDrawElementsInstanced(drawMode, indexCount, GL_UNSIGNED_INT, 0, instanceCount);
    else
      glDrawArraysInstanced(drawMode, 0, vertexCount, instanceCount);
  }

  void GrassMesh::GenerateInstanceMeshData(std::vector<GLfloat> & vertexData, 
    std::vector<VertexAttribute> & vertextAttributes, int & vertexDataStride, int & instanceCount)
  {
    instanceCount = 0;
    vertextAttributes.push_back(VertexAttribute(12, 4, 0));
    vertexDataStride = sizeof(GLfloat) * 4;

    float xStart = -30.0f;
    float xStop = 30.0f;
    float xStep = 1.0f;
    float xRandomD = 0.5f;
    float zStart = -30.0f;
    float zStop = 30.0f;
    float zStep = 1.0f;
    float zRandomD = 0.5f;
    for (float x = xStart; x < xStop; x += xStep)
    {
      for (float z = zStart; z < zStop; z += zStep)
      {
        // pos
        float xr = (float)rand() / RAND_MAX;
        vertexData.push_back(x + xr * xStep * xRandomD);
        float zr = (float)rand() / RAND_MAX;
        vertexData.push_back(z + zr * zStep * zRandomD);
        // scale
        float sr = (float)rand() / RAND_MAX;
        vertexData.push_back(1.0f + sr * 1.0f);
        // rot
        float rr = (float)rand() / RAND_MAX;
        vertexData.push_back(rr * 3.14f * 2);
        instanceCount++;
      }
    }
  }

}
