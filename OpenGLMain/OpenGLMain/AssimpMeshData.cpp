#include "AssimpMeshData.h"

#include <iostream>

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>


namespace engine
{
  void AssimpMeshData::ParseMeshes(const std::string & fileName, std::vector<std::shared_ptr<MeshData>> & result)
  {
    Assimp::Importer importer;
    auto scene = importer.ReadFile(fileName.c_str(), aiProcess_Triangulate | aiProcess_GenNormals);
    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
      std::cerr 
        << "file:" << __FILE__ << std::endl
        << "func:" << "ParseMeshes" << std::endl
        << "assimp error:" << importer.GetErrorString() << std::endl
        << "file:" << fileName << std::endl
        << std::endl << std::endl;
      return;
    }

    ProcessNode(scene, scene->mRootNode, result);
  }

  void AssimpMeshData::ProcessNode(const aiScene * scene, const aiNode * node, std::vector<std::shared_ptr<MeshData>> & result)
  {
    for (int mIdx = 0; mIdx < node->mNumMeshes; ++mIdx)
    {
      const aiMesh * mesh = scene->mMeshes[node->mMeshes[mIdx]];
      result.push_back(std::shared_ptr<MeshData>((MeshData *)new AssimpMeshData(mesh)));
    }

    for (int nIdx = 0; nIdx < node->mNumChildren; ++nIdx)
    {
      ProcessNode(scene, node->mChildren[nIdx], result);
    }
  }

  AssimpMeshData::AssimpMeshData(const aiMesh * mesh)
  {
    // vertex
    // pos
    if (mesh->HasPositions())
      PushBackVertexAttribute(VertexAttribute::POS_IDX, 3);
    // normal
    if (mesh->HasNormals())
      PushBackVertexAttribute(VertexAttribute::NORM_IDX, 3);
    // uv
    if (mesh->HasTextureCoords(0))
      PushBackVertexAttribute(VertexAttribute::UV_IDX, 2);

    for (int vIdx = 0; vIdx < mesh->mNumVertices; ++vIdx)
    {
      // pos
      if (mesh->HasPositions())
      {
        vertexData.push_back(mesh->mVertices[vIdx].x);
        vertexData.push_back(mesh->mVertices[vIdx].y);
        vertexData.push_back(mesh->mVertices[vIdx].z);
      }

      // normal
      if (mesh->HasNormals())
      {
        vertexData.push_back(mesh->mNormals[vIdx].x);
        vertexData.push_back(mesh->mNormals[vIdx].y);
        vertexData.push_back(mesh->mNormals[vIdx].z);
      }

      // tbn

      // uv
      if (mesh->HasTextureCoords(0))
      {
        vertexData.push_back(mesh->mTextureCoords[0][vIdx].x);
        vertexData.push_back(mesh->mTextureCoords[0][vIdx].y);
      }

      // color
    }

    vertexCount = mesh->mNumVertices;

    // index
    isIndexDataExist = mesh->HasFaces();
    if (isIndexDataExist)
    {
      for (int fIdx = 0; fIdx < mesh->mNumFaces; ++fIdx)
      {
        auto face = mesh->mFaces[fIdx];
        for (int iIdx = 0; iIdx < face.mNumIndices; ++iIdx)
        {
          indexData.push_back(face.mIndices[iIdx]);
        }
      }
    }

    // draw mode
    drawMode = GL_TRIANGLES;
  }


  AssimpMeshData::~AssimpMeshData()
  {
  }
}
