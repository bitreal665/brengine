#include "Shader.h"

#include <iostream>


namespace engine
{
  Shader::Shader(std::shared_ptr<ShaderData> _vertShaderData, std::shared_ptr<ShaderData> _geomShaderData, std::shared_ptr<ShaderData> _fragShaderData) :
    
    cullFace(true),
    cullFaceMode(GL_BACK),

    zWrite(true),
    zFunc(GL_LESS),

    blend(false),
    // ���� ���-���� ����� �������, ����������� ��� � �����-��������
    rgbSFactor(GL_SRC_ALPHA),
    rgbDFactor(GL_ONE_MINUS_SRC_ALPHA),
    rgbBlendQuation(GL_FUNC_ADD),
    aSFactor(GL_SRC_ALPHA),
    aDFactor(GL_ONE_MINUS_SRC_ALPHA),
    aBlendQuation(GL_FUNC_ADD),

    stencil(false),
    //
    stencilFunc(GL_NEVER),
    stencilRef(0),
    stencilMask(0xFF),
    stencilSFailOp(GL_KEEP),
    stencilDFailOp(GL_KEEP),
    stencilPassOp(GL_KEEP),

    vertShaderData(_vertShaderData),
    geomShaderData(_geomShaderData),
    fragShaderData(_fragShaderData),

    renderPassName("forward"),
    order(0)
	{
    // vertex
    GLuint vertexShader = CreateShader(GL_VERTEX_SHADER, _vertShaderData);
    // geom
    GLuint geomShader = _geomShaderData ? CreateShader(GL_GEOMETRY_SHADER, _geomShaderData) : 0;
    // fragment
    GLuint fragmentShader = CreateShader(GL_FRAGMENT_SHADER, _fragShaderData);
    // shader
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    if (geomShader != 0)
      glAttachShader(shaderProgram, geomShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    GLint success;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success)
    {
      GLchar infoLog[512];
      glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
      std::cerr
        << "file:" << __FILE__ << std::endl
        << "func:" << "Init" << std::endl
        << "glLinkProgram error:" << infoLog << std::endl
        << "v shader src file:" << _vertShaderData->GetFileName() << std::endl
        << "g shader src file:" << (_geomShaderData ? _geomShaderData->GetFileName() : "<no g shader>") << std::endl
        << "f shader src file:" << _fragShaderData->GetFileName() << std::endl
        << std::endl << std::endl;
    }

    // clear vertex and fragment
    glDeleteShader(vertexShader);
    if (geomShader != 0)
      glDeleteShader(geomShader);
    glDeleteShader(fragmentShader);

    std::cout << "Shader++" << std::endl;
	}

	Shader::~Shader()
  {
    glDeleteProgram(shaderProgram);

    std::cout << "Shader--" << std::endl;
	}

  GLuint Shader::CreateShader(GLenum shaderType, std::shared_ptr<ShaderData> shaderData)
  {
    GLuint shader;
    shader = glCreateShader(shaderType);
    const GLchar * shaderSrc = shaderData->GetShaderSrc();
    glShaderSource(shader, 1, &shaderSrc, nullptr);
    glCompileShader(shader);
    GLint success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
      GLchar infoLog[512];
      glGetShaderInfoLog(shader, 512, nullptr, infoLog);
      std::cerr
        << "file:" << __FILE__ << std::endl
        << "func:" << "CreateShader" << std::endl
        << "glCompileShader error:" << infoLog << std::endl
        << "shader src file:" << shaderData->GetFileName() << std::endl
        << "shader src:" << std::endl << shaderSrc << std::endl
        << std::endl << std::endl;
    }
    return shader;
  }
}
