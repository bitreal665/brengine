#pragma once

#include <memory>

#include <glm.hpp>

#include "Component.h"


namespace engine
{
  class Shader;
  class Material;
  class AbstractMesh;

  class BaseLight : public Component
  {
    DEFINE_COMPONENT(BaseLight, Component)

  public:
    static void LoadDebuResources();
    static void UnloadDebuResources();

    glm::vec4 color;

    BaseLight();
    ~BaseLight() override;

    void SetGameObject(GameObject * _gameObject) override;
    void Update(const RenderingContext & context) override;

  private:
    static std::shared_ptr<Shader> markerShader;
    static std::shared_ptr<AbstractMesh> markerMesh;

    std::shared_ptr<Material> markerMaterial;
  };
}
