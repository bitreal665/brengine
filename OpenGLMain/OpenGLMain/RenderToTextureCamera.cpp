#include "RenderToTextureCamera.h"

#include <iostream>

#include "RenderTexture.h"


namespace engine
{
  DECLARE_COMPONENT(RenderToTextureCamera)

  RenderToTextureCamera::RenderToTextureCamera(float fov, float w, float h, float near, float far) :
    BaseCamera(fov, w, h, near, far)
  {
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    // create a color attachment texture
    texture = std::shared_ptr<AbstractTexture>((AbstractTexture *)new RenderTexture(w, h, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE));
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->GetTexture(), 0);
    // create a renderbuffer object for depth and stencil attachment (we won't be sampling these)
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, w, h); // use a single renderbuffer object for both a depth AND stencil buffer.
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo); // now actually attach it
                                                                                                  // now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cerr
        << "file:" << __FILE__ << std::endl
        << "func:" << "RenderToTextureCamera" << std::endl
        << "fbo creating error:" << "" << std::endl
        << std::endl << std::endl;
    }

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glEnable(GL_DEPTH_TEST);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  RenderToTextureCamera::~RenderToTextureCamera()
  {
    glDeleteRenderbuffers(1, &rbo);
    glDeleteFramebuffers(1, &framebuffer);
  }

  void RenderToTextureCamera::Render(RenderingContext & context, const std::vector<std::shared_ptr<GameObject>> & objects) const
  {
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_TRUE);
    glStencilMask(0xFF);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    BaseCamera::Render(context, objects);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  std::shared_ptr<AbstractTexture> RenderToTextureCamera::GetTexture() const
  {
    return texture;
  }
}