#include "RenderTexture.h"

#include <iostream>


namespace engine
{
  RenderTexture::RenderTexture(int w, int h, GLint internalFormat, GLenum foramt, GLenum type)
  {
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0, foramt, type, nullptr);
    glBindTexture(GL_TEXTURE_2D, 0);

    std::cout << "RenderTexture++" << std::endl;
  }


  RenderTexture::~RenderTexture()
  {
    std::cout << "RenderTexture++" << std::endl;
  }
}