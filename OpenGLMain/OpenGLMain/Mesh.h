#pragma once

#include <memory>

#include "MeshData.h"
#include "AbstractMesh.h"

namespace engine
{
  class Mesh : AbstractMesh
  {
  public:
    Mesh(std::shared_ptr<MeshData> _meshData);
    virtual ~Mesh();

    void Render(RenderingContext & renderingContext) const override;

  protected:
    std::weak_ptr<MeshData> meshData;
    GLuint VAO;
    GLuint VBO;
    GLuint EBO;
    int vertexCount;
    int indexCount;
    GLenum drawMode;

    virtual void InternalRender(RenderingContext & renderingContext) const;
  };
}

