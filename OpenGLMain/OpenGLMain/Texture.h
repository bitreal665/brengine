#pragma once

#include <memory>

#include "TextureData.h"
#include "AbstractTexture.h"

namespace engine
{
	class Texture : AbstractTexture
	{
	public:
		Texture(std::shared_ptr<TextureData> _textureData, GLint clampMode, GLenum format);
		~Texture();

    GLuint GetTexture() const override { return texture; };
    GLuint GetTextureType() const override { return GL_TEXTURE_2D; };

	private:
    std::weak_ptr<TextureData> textureData;
    GLuint texture;
	};
}

