#pragma once

#include <memory>

#include "TextureData.h"
#include "AbstractTexture.h"

namespace engine
{
  class CubeMapTexture : AbstractTexture
  {
  public:
    CubeMapTexture(
      std::shared_ptr<TextureData> _posXTextureData,
      std::shared_ptr<TextureData> _negXTextureData, 
      std::shared_ptr<TextureData> _posYTextureData, 
      std::shared_ptr<TextureData> _negYTextureData,
      std::shared_ptr<TextureData> _posZTextureData, 
      std::shared_ptr<TextureData> _negZTextureData);
    ~CubeMapTexture(); 
    
    GLuint GetTexture() const override { return texture; };
    GLuint GetTextureType() const override { return GL_TEXTURE_CUBE_MAP; };

  private:
    std::weak_ptr<TextureData> posXTextureData;
    std::weak_ptr<TextureData> negXTextureData;
    std::weak_ptr<TextureData> posYTextureData;
    std::weak_ptr<TextureData> negYTextureData;
    std::weak_ptr<TextureData> posZTextureData;
    std::weak_ptr<TextureData> negZTextureData;
    GLuint texture;
  };
}

