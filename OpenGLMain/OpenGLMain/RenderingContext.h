#pragma once

#include <string>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm.hpp>


namespace engine
{
	class RenderingContext
	{
	public:
		GLFWwindow * window;
		float deltaTime;
    float time;

    std::string renderPassName;

		glm::mat4x4 modelTransformationMatrix;
    glm::mat3x3 normalModelTransformationMatrix;

    std::string debugOutput;
	};
}
