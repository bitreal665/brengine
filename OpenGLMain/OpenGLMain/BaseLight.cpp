#include "BaseLight.h"

#include "GameObject.h"
#include "Transform.h"
#include "Renderer.h"
#include "CubeMeshData.h"
#include "TextureData.h"
#include "ShaderData.h"
#include "AbstractMesh.h"
#include "Mesh.h"
#include "Shader.h"
#include "Material.h"


namespace engine
{
  DECLARE_COMPONENT(BaseLight)

  std::shared_ptr<Shader> BaseLight::markerShader;
  std::shared_ptr<AbstractMesh> BaseLight::markerMesh;

  void BaseLight::LoadDebuResources()
  {
    auto meshData = std::shared_ptr<MeshData>((MeshData *)new CubeMeshData(0.3f));
    auto vShaderData = std::shared_ptr<ShaderData>(new ShaderData("Data/Shaders/pos_norm_color_uv.vert"));
    auto fShaderData = std::shared_ptr<ShaderData>(new ShaderData("Data/Shaders/ucolor.frag"));

    markerMesh = std::shared_ptr<AbstractMesh>((AbstractMesh *)new Mesh(meshData));

    markerShader = std::shared_ptr<Shader>(new Shader(vShaderData, std::shared_ptr<ShaderData>(), fShaderData));
    markerShader->SetRenderPassName("forward");
    markerShader->SetOrder(0);
  }

  void BaseLight::UnloadDebuResources()
  {
    markerShader.reset();
    markerMesh.reset();
  }

  BaseLight::BaseLight() :
    color(1.0f, 1.0f, 1.0f, 1.0f)
  {
    markerMaterial = std::shared_ptr<Material>(new Material());
    markerMaterial->AddShader(markerShader);
  }

  BaseLight::~BaseLight()
  {
  }

  void BaseLight::SetGameObject(GameObject * _gameObject)
  {
    Component::SetGameObject(_gameObject);

    auto r = new Renderer(markerMaterial, markerMesh);
    r->SetLayer("debug");
    GetGameObject()->AddComponent((Component *)r);
  }

  void BaseLight::Update(const RenderingContext & context)
  {
    Component::Update(context);
    markerMaterial->UpdateUVec4("col", color);
  }
}
