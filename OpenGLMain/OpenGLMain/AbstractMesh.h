#pragma once

#include "RenderingContext.h"


namespace engine
{
	class AbstractMesh
	{
	public:
		void virtual Render(RenderingContext & context) const = 0;
	};
}

