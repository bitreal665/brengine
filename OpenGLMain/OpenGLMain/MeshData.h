#pragma once

#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>

namespace engine
{
  // ��� glVertexAttribPointer
  struct VertexAttribute
  {
    static const int POS_IDX = 0;
    static const int NORM_IDX = 1;
    static const int TANGENT_IDX = 2;
    static const int BITANGENT_IDX = 3;
    static const int COLOR_IDX = 4;
    static const int UV_IDX = 8;

    int index;  // ������ � �������
    int size; // ������ (���������� ���������)
    int offset; // �������� (� ������)

    VertexAttribute::VertexAttribute(int _index, int _size, int _offset) :
      index(_index),
      size(_size),
      offset(_offset)
    {

    }
  };

  class MeshData
  {
  public:
    MeshData();
    virtual ~MeshData();

    const std::vector<GLfloat> & GetVertexData() const;
    const std::vector<VertexAttribute> & GetVertextAttributes() const;
    int GetVertexCount() const;
    int GetVertexDataStrid() const;

    bool IsIndexDataExist() const;
    const std::vector<GLuint> & GetIndexData() const;
    int getIndexCount() const;

    GLenum GetDrawMode() const;

  protected:
    std::vector<GLfloat> vertexData;
    std::vector<VertexAttribute> vertexAttributes;
    int vertexCount;
    int vertexDataStrid;

    bool isIndexDataExist;
    std::vector<GLuint> indexData;

    GLenum drawMode;

    void PushBackVertexAttribute(int index, int size);
  };
}

