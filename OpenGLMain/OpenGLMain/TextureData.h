#pragma once

#include <string>


namespace engine
{
  class TextureData
  {
  public:
    TextureData(const std::string & fileName, int format);
    ~TextureData();

    int GetWidth() const { return width; }
    int GetHeight() const { return height; }
    int GetFormat() const { return format; }
    const unsigned char * GetTextureBytes() const { return image; }

  private:
    int width;
    int height;
    int format;
    unsigned char * image;
  };
}

