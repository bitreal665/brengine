#include "RotationAnimation.h"

#include "GameObject.h"
#include "Transform.h"


namespace engine
{
  DECLARE_COMPONENT(RotationAnimation)

  RotationAnimation::RotationAnimation() :
    speed(0.0f, 0.0f, 0.0f)
  {
  }

  RotationAnimation::~RotationAnimation()
  {
  }

  void RotationAnimation::Update(const RenderingContext & context)
  {
    auto cRot = GetGameObject()->FindComponent<Transform>()->GetRotation();
    cRot += speed * context.deltaTime;
    GetGameObject()->FindComponent<Transform>()->SetRotation(cRot);
  }
}
