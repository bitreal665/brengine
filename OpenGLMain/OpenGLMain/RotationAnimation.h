#pragma once

#include "Component.h"

#include <glm.hpp>

namespace engine
{
  class RotationAnimation : public Component
  {
    DEFINE_COMPONENT(RotationAnimation, Component)

  public:
    glm::vec3 speed;

    RotationAnimation();
    ~RotationAnimation() override;

    void Update(const RenderingContext & context) override;
  };
}

