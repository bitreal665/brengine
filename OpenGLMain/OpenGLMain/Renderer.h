#pragma once

#include <memory>

#include "Component.h"
#include "Material.h"
#include "AbstractMesh.h"


namespace engine
{
	class Renderer : public Component
	{
    DEFINE_COMPONENT(Renderer, Component)

	public:
		Renderer(std::shared_ptr<Material> _material, std::shared_ptr<engine::AbstractMesh> _mesh);
		~Renderer() override;

    const std::string & GetLayer() const { return layer; }
    void SetLayer(const std::string & _layer) { layer = _layer; }

    std::shared_ptr<Material> GetMaterial() const { return material; }

		void Render(RenderingContext & context) const;

	private:
		std::shared_ptr<Material> material;
		std::shared_ptr<AbstractMesh> mesh;
    std::string layer;
	};
}
