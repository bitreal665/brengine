#include "Device.h"
#include <iostream>
#include <algorithm>


namespace VkTutor
{
  Device::Device()
  {
    
  }

  Device::~Device()
  {
  }

  void Device::Init(const VkPhysicalDevice & _vkDevice, const VkSurfaceKHR & _surface)
  {
    vkDevice = _vkDevice;

    // DEVICE INFO
    // properties
    vkGetPhysicalDeviceProperties(vkDevice, &properties);
    // features
    vkGetPhysicalDeviceFeatures(vkDevice, &features);
    // extensions
    uint32_t availableExtensionsCount;
    vkEnumerateDeviceExtensionProperties(vkDevice, nullptr, &availableExtensionsCount, nullptr);
    availableExtensions.resize(availableExtensionsCount);
    vkEnumerateDeviceExtensionProperties(vkDevice, nullptr, &availableExtensionsCount, availableExtensions.data());
    availableExtensionsNames.resize(availableExtensionsCount);
    for (uint32_t i = 0; i < availableExtensionsCount; ++i)
      availableExtensionsNames[i] = availableExtensions[i].extensionName;

    // SURFACE
    // surface capabilities
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vkDevice, _surface, &surfCapabilities);
    // surface formats
    uint32_t surfFormatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(vkDevice, _surface, &surfFormatCount, nullptr);
    if (surfFormatCount != 0)
    {
      surfFormats.resize(surfFormatCount);
      vkGetPhysicalDeviceSurfaceFormatsKHR(vkDevice, _surface, &surfFormatCount, surfFormats.data());
    }
    // surface present modes
    uint32_t surfPresentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(vkDevice, _surface, &surfPresentModeCount, nullptr);
    if (surfPresentModeCount != 0)
    {
      surfPresentModes.resize(surfPresentModeCount);
      vkGetPhysicalDeviceSurfacePresentModesKHR(vkDevice, _surface, &surfPresentModeCount, surfPresentModes.data());
    }

    // QUEUE FAMILIES
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(vkDevice, &queueFamilyCount, nullptr);
    queueFamilies.resize(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(vkDevice, &queueFamilyCount, queueFamilies.data());
  }

  void Device::PrintInfo(const std::string & _prefix) const
  {
    std::cout
      << _prefix << "name: " << properties.deviceName << std::endl
      << _prefix << "type: " << properties.deviceType << std::endl
      << _prefix << "available extensions: " << std::endl;
    for (auto & extension : availableExtensionsNames)
      std::cout << _prefix << "\t" << extension << std::endl;
    std::cout
      << _prefix << "available surf present modes: " << std::endl;
    for (auto presentMode : surfPresentModes)
    {
      if (presentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
        std::cout << _prefix << "\t" << "VK_PRESENT_MODE_IMMEDIATE_KHR" << std::endl;
      else if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR)
        std::cout << _prefix << "\t" << "VK_PRESENT_MODE_MAILBOX_KHR" << std::endl;
      else if (presentMode == VK_PRESENT_MODE_FIFO_KHR)
        std::cout << _prefix << "\t" << "VK_PRESENT_MODE_FIFO_KHR" << std::endl;
      else if (presentMode == VK_PRESENT_MODE_FIFO_RELAXED_KHR)
        std::cout << _prefix << "\t" << "VK_PRESENT_MODE_FIFO_RELAXED_KHR" << std::endl;
      else
        std::cout << _prefix << "\t" << "other" << std::endl;
    }
    std::cout << _prefix << "available queue families: " << std::endl;
    for (size_t i = 0; i < queueFamilies.size(); ++i)
    {
      std::cout
        << _prefix << "\t" << "idx: " << i << std::endl
        << _prefix << "\t" << "flags: " << std::endl;
      if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        std::cout << _prefix << "\t\t" << "VK_QUEUE_GRAPHICS_BIT" << std::endl;
      if (queueFamilies[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
        std::cout << _prefix << "\t\t" << "VK_QUEUE_COMPUTE_BIT " << std::endl;
      if (queueFamilies[i].queueFlags & VK_QUEUE_TRANSFER_BIT)
        std::cout << _prefix << "\t\t" << "VK_QUEUE_TRANSFER_BIT " << std::endl;
      if (queueFamilies[i].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT)
        std::cout << _prefix << "\t\t" << "VK_QUEUE_SPARSE_BINDING_BIT " << std::endl;
    }
  }

  void Device::Select(
    int _windowWidth, 
    int _windowHeight,
    const VkSurfaceKHR & _surface)
  {
    //
    if (surfCapabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
    {
      selectedSurfExtent = surfCapabilities.currentExtent;
    }
    else
    {
      selectedSurfExtent.width = std::max(surfCapabilities.minImageExtent.width, std::min(surfCapabilities.maxImageExtent.width, static_cast<uint32_t>(_windowWidth)));
      selectedSurfExtent.height = std::max(surfCapabilities.minImageExtent.height, std::min(surfCapabilities.maxImageExtent.height, static_cast<uint32_t>(_windowHeight)));
    }
    //
    selectedSurfImageCount = surfCapabilities.minImageCount + 1;
    if (surfCapabilities.maxImageCount > 0 && selectedSurfImageCount > surfCapabilities.maxImageCount)
      selectedSurfImageCount = surfCapabilities.maxImageCount;
    //
    if (surfFormats.size() == 1 && surfFormats[0].format == VK_FORMAT_UNDEFINED)
    {
      selectedSurfFormat.format = VK_FORMAT_B8G8R8A8_UNORM;
      selectedSurfFormat.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
    }
    else
    {
      if (!surfFormats.empty())
      {
        selectedSurfFormat.format = surfFormats[0].format;
        selectedSurfFormat.colorSpace = surfFormats[0].colorSpace;
      }

      for (auto & format : surfFormats)
      {
        if (format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
          selectedSurfFormat.format = VK_FORMAT_B8G8R8A8_UNORM;
          selectedSurfFormat.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
          break;
        }
      }
    }
    //
    if (std::find(surfPresentModes.begin(), surfPresentModes.end(), VK_PRESENT_MODE_MAILBOX_KHR) != surfPresentModes.end())
      selectedSurfPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    else if (std::find(surfPresentModes.begin(), surfPresentModes.end(), VK_PRESENT_MODE_FIFO_KHR) != surfPresentModes.end())
      selectedSurfPresentMode = VK_PRESENT_MODE_FIFO_KHR;
    else if (std::find(surfPresentModes.begin(), surfPresentModes.end(), VK_PRESENT_MODE_IMMEDIATE_KHR) != surfPresentModes.end())
      selectedSurfPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
    else if (!surfPresentModes.empty())
      selectedSurfPresentMode = surfPresentModes[0];
    //
    for (size_t i = 0; i < queueFamilies.size(); ++i)
    {
      //
      VkBool32 presentSupport = false;
      vkGetPhysicalDeviceSurfaceSupportKHR(vkDevice, i, _surface, &presentSupport);
      if (presentSupport)
        presentFamily = i;
      //
      bool isQueueFamily = queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT;
      if (isQueueFamily)
        graphicsFamily = i;
    }
  }

  bool Device::IsSuitable(const std::vector<const char *> & _requiredExtensions) const
  {
    // TODO: � �� ��� ��� strcmp �������� 2 ������� const char *
    std::set<std::string> _available;
    for (auto ext : availableExtensionsNames)
      _available.insert(ext);
    std::vector<std::string> _required;
    for (auto ext : _requiredExtensions)
      _required.push_back(ext);
    for (auto & ext : _required)
      if (std::find(_available.begin(), _available.end(), ext) == _available.end())
        return false;

    return 
      !surfFormats.empty()
      && !surfPresentModes.empty()
      && graphicsFamily >= 0
      && presentFamily >= 0;
  }
}