#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <set>
#include <string>

namespace VkTutor
{
  class Device
  {
  public:
    Device();
    ~Device();

    void Init(
      const VkPhysicalDevice & _vkDevice,
      const VkSurfaceKHR & _surface);

    void PrintInfo(const std::string & _prefix) const;

    void Select(
      int _windowWidth,
      int _windowHeight,
      const VkSurfaceKHR & _surface);

    bool IsSuitable(const std::vector<const char *> & _requiredExtensions) const;

    const VkPhysicalDevice & VkDevice() const { return vkDevice; }
    int GraphicsFamily() const { return graphicsFamily; }
    int PresentFamily() const { return presentFamily; }
    const std::vector<const char *> & AvailableExtensionsNames() const { return availableExtensionsNames; }
    const VkExtent2D & SelectedSurfExtent() const { return selectedSurfExtent; }
    uint32_t SelectedSurfImageCount() const { return selectedSurfImageCount; }
    const VkSurfaceFormatKHR & SelectedSurfFormat() const { return selectedSurfFormat; }
    VkPresentModeKHR SelectedSurfPresentMode() const { return selectedSurfPresentMode; }

  private:
    VkPhysicalDevice vkDevice;

    VkPhysicalDeviceProperties properties;
    VkPhysicalDeviceFeatures features;
    std::vector<VkExtensionProperties> availableExtensions;
    std::vector<const char *> availableExtensionsNames;

    VkSurfaceCapabilitiesKHR surfCapabilities;
    std::vector<VkSurfaceFormatKHR> surfFormats;
    std::vector<VkPresentModeKHR> surfPresentModes;

    std::vector<VkQueueFamilyProperties> queueFamilies;

    VkExtent2D selectedSurfExtent;
    uint32_t selectedSurfImageCount;
    VkSurfaceFormatKHR selectedSurfFormat;
    VkPresentModeKHR selectedSurfPresentMode;

    int graphicsFamily = -1;
    int presentFamily = -1;
  };
}

