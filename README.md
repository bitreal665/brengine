This project is a simple 3d graphics engine that uses OpenGL 3.

Short video - https://youtu.be/NT2cNxtHIcs

I started this project because I wanted to improve my OpenGL and C++ knowledge. I used amazing Joey de Vries's tutorial https://learnopengl.com/.

# How to build

You need Visual Studio 2017+

VS Solution has 2 projects - OpenGLMain and VulkanMain. Build OpenGLMain please.

Build x86 please.

# How to play

w/a/s/d - move forward/left/back/right

shift - move upwards

ctrl - move downwards

pressed space - look around by mouse

esc - exit

# Features

## Deferred rendeing

![gbuffer picture](ImagesForReadme/gbuffer.png)

## GPU instancing

![gpuInstancing picture](ImagesForReadme/gpuInstancing.png)

## Render in texture

![renderingInTexture picture](ImagesForReadme/renderingInTexture.png)

## Different types of lights

![light picture](ImagesForReadme/light.png)

![light2 picture](ImagesForReadme/light2.png)

## Display of normals by geometry shader

![normalsByGeomShader picture](ImagesForReadme/normalsByGeomShader.png)

# About code

Start watching from main.cpp. There are engine initialization, compiling of a 3d scene and the main loop.

The engine has Unity3D like API (GameObjects and Components). Classes ended of \*Data are raw data for resources (it's string for shaders, or it's array of atributes for meshes, ...).